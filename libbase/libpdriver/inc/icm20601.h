/*!******************************************************************************
 * @file    ICM20601.h
 * @brief   ICM20601 sensor driver header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 * @par     Data Sheet
 *          MC3413 3-Axis Accelerometer Data Sheet APS-048-0029v1.7
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ICM20601_H__
#define __ICM20601_H__
#include "icm20601_api.h"


// Full scale
#define ICM20601_SCALE_ACCEL				((float)(1.)) // �}10g
#define ICM20601_SCALE_GYRO					((float)(1.) )// �}2000dps
/*
 *
 *		By environment, you must edit this file
 *
 */
// driver parameter
#define ICM20601_ADDRESS		     (0x69)	// or (0x68) I2C device address b0110_100X

/*  register map*/
#define ICM20601_SELF_TEST_X_GYRO		(0x00) // XG_ST_DATA[7:0]
#define ICM20601_SELF_TEST_Y_GYRO		(0x01) // YG_ST_DATA[7:0]
#define ICM20601_SELF_TEST_Z_GYRO		(0x02) // ZG_ST_DATA[7:0]
#define ICM20601_SELF_TEST_X_ACCEL		(0x0D) // XA_ST_DATA[7:0]
#define ICM20601_SELF_TEST_Y_ACCEL		(0x0E) // YA_ST_DATA[7:0]
#define ICM20601_SELF_TEST_Z_ACCEL		(0x0F) // ZA_ST_DATA[7:0]

#define ICM20601_XG_OFFS_USRH			(0x13) // X_OFFS_USR [15:8]
#define ICM20601_XG_OFFS_USRL  	     	(0x14) // X_OFFS_USR [7:0]
#define ICM20601_YG_OFFS_USRH		 	(0x15) // Y_OFFS_USR [15:8]
#define ICM20601_YG_OFFS_USRL		 	(0x16) // Y_OFFS_USR [7:0]
#define ICM20601_ZG_OFFS_USRH	 		(0x17) // Z_OFFS_USR [15:8]
#define ICM20601_ZG_OFFS_USRL		 	(0x18) // Z_OFFS_USR [7:0]
#define ICM20601_SMPLRT_DIV	  			(0x19) // SMPLRT_DIV[7:0]
#define ICM20601_CONFIG					(0x1A) //
#define ICM20601_GYRO_CONFIG		 	(0x1B) //
#define ICM20601_ACCEL_CONFIG	  	 	(0x1C) //
#define ICM20601_ACCEL_CONFIG2			(0x1D) //
#define ICM20601_LP_MODE_CFG  	     	(0x1E) //
#define ICM20601_ACCEL_WOM_THR		 	(0x1F) // WOM_THR[7:0]
#define ICM20601_FIFO_EN			 	(0x23) //
#define ICM20601_FSYNC_INT	 			(0x36) //
#define ICM20601_INT_PIN_CFG		 	(0x37) //
#define ICM20601_INT_ENABLE				(0x38) //
#define ICM20601_INT_STATUS		 		(0x3A) //

#define ICM20601_ACCEL_XOUT_H			(0x3B) // ACCEL_XOUT_H[15:8]
#define ICM20601_ACCEL_XOUT_L			(0x3C) // ACCEL_XOUT_L[7:0]
#define ICM20601_ACCEL_YOUT_H			(0x3D) // ACCEL_YOUT_H[15:8]
#define ICM20601_ACCEL_YOUT_L			(0x3E) // ACCEL_YOUT_L[7:0]
#define ICM20601_ACCEL_ZOUT_H			(0x3F) // ACCEL_ZOUT_H[15:8]
#define ICM20601_ACCEL_ZOUT_L			(0x40) // ACCEL_ZOUT_L[7:0]
#define ICM20601_TEMP_OUT_H				(0x41) // TEMP_OUT[15:8]
#define ICM20601_TEMP_OUT_L  	     	(0x42) // TEMP_OUT[7:0]
#define ICM20601_GYRO_XOUT_H		 	(0x43) // GYRO_XOUT[15:8]
#define ICM20601_GYRO_XOUT_L		 	(0x44) // GYRO_XOUT[7:0]
#define ICM20601_GYRO_YOUT_H	 		(0x45) // GYRO_YOUT[15:8]
#define ICM20601_GYRO_YOUT_L		 	(0x46) // GYRO_YOUT[7:0]
#define ICM20601_GYRO_ZOUT_H	  		(0x47) // GYRO_ZOUT[15:8]
#define ICM20601_GYRO_ZOUT_L			(0x48) // GYRO_ZOUT[7:0]

#define ICM20601_SIGNAL_PATH_RESET		(0x68) //
#define ICM20601_ACCEL_INTEL_CTRL	  	(0x69) //
#define ICM20601_USER_CTRL				(0x6A) //
#define ICM20601_PWR_MGMT_1  	     	(0x6B) //
#define ICM20601_PWR_MGMT_2			 	(0x6C) //
#define ICM20601_FIFO_COUNTH		 	(0x72) // FIFO_COUNT[7:0]
#define ICM20601_FIFO_COUNTL	 		(0x73) // FIFO_DATA[7:0]
#define ICM20601_FIFO_R_W		 		(0x74) //

// product code
#define ICM20601_WHO_AM_I				(0x75) // WHOAMI[7:0]
#define ICM20601_WHO_AM_I_ID     		(0xAC) // Device ID Number 0xAC or 0x68 or 0x69



#define ICM20601_XA_OFFSET_H	 		(0x77) // XA_OFFS [14:7]
#define ICM20601_XA_OFFSET_L	  	 	(0x78) //
#define ICM20601_YA_OFFSET_H   		    (0x7A) // YA_OFFS [14:7]
#define ICM20601_YA_OFFSET_L 	     	(0x7B) //
#define ICM20601_ZA_OFFSET_H 	  	  	(0x7D) // ZA_OFFS [14:7]
#define ICM20601_ZA_OFFSET_L   		   	(0x7E) //


// state
#define ICM20601_SMPLRT			(0x00)	// 1kHz [0000 0000]	REGISTER 25
#define ICM20601_CFG			(0x01)	// 3-dB BW 176Hz, Noise BW 177Hz, Rate 1kHz [0000 0001] REGISTER 26
#define ICM20601_GYRO_CFG		(0x10)	// 2000dps	[0001 0000]	REGISTER 27
#define ICM20601_ACCEL_CFG		(0x10)	// 16g		[0001 0000] REGISTER 28
#define ICM20601_ACCEL_CFG2		(0x08)	// 3-db BW 420Hz, Noise BW 441.6Hz, Rate 1kHz [0000 0100] REGISTER 29
#define ICM20601_FIFO_ENABLE	(0x78)	// [0111 1000]

#define ICM20601_MODE_STANDBY	(0x00)	// standby
#define ICM20601_MODE_ACTIVE	(0x3F)	// active

//read & write register
#define ICM20601_WRITE			(0x00)
#define ICM20601_READ			(0x01)

#endif

