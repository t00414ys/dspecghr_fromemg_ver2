/*!******************************************************************************
 * @file    ads1191_api.h
 * @brief   ads1191 sensor api header
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ADXL362_API_H__
#define __ADXL362_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int ads1191_init( unsigned int param );
void ads1191_ctrl_accl( int f_ena );
unsigned int ads1191_rcv_accl( unsigned int tick );
int ads1191_conv_accl( frizz_fp data[7] );

int ads1191_setparam_accl( void *ptr );
unsigned int ads1191_get_ver( void );
unsigned int ads1191_get_name( void );

int ads1191_get_condition( void *data );
int ads1191_get_raw_data( void *data );

#ifdef __cplusplus
}
#endif


#endif

