/*
 * lsm330_api.h
 *
 *  Created on: 2014/11/24
 *      Author: lynetteli
 */

#ifndef __LSM330_API_H__
#define __LSM330_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int lsm330_init( unsigned int param );
void lsm330_ctrl_accl( int f_ena );
void lsm330_ctrl_gyro( int f_ena );
unsigned int lsm330_rcv_accl( unsigned int tick );
unsigned int lsm330_rcv_gyro( unsigned int tick );
int lsm330_conv_accl( frizz_fp data[3] );
int lsm330_conv_gyro( frizz_fp data[4] );

int lsm330_gyro_get_condition( void *data );
int lsm330_accl_get_condition( void *data );
int lsm330_accl_get_raw_data( void *data );

int lsm330_setparam_accl( void *ptr );
int lsm330_setparam_gyro( void *ptr );
unsigned int lsm330_get_ver( void );
unsigned int lsm330_get_name( void );

#ifdef __cplusplus
}
#endif


#endif /*  */
