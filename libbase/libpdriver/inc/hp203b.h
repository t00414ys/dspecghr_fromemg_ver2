/*!******************************************************************************
 * @file    hp203b.h
 * @brief   hp203b sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __PRESS_HP203B_H__
#define __PRESS_HP203B_H__

#include "hp203b_api.h"

/*
 *
 *		The rest of this is okay not change
 *
 */
#define HP203B_SCALE		(1.0/100.0)


#define HP203B_DEVICE_ID_1					0x80
#define HP203B_DEVICE_ID_2					0x81

#define hp20x_I2C_ADDRESS_H 				0x76
#define hp20x_I2C_ADDRESS_L 				0x77

//----------------------------------
#define hp20x_WHO_AM_I						0x8F

#define hp20x_REG_RESET_CMD					0x06
#define hp20x_REG_INT_SRC					0x0D
#define hp20x_TEMP_PRESSURE_MEASUREMENT		0x10
#define hp20x_TEMP_ALTITUDE_MEASUREMENT		0x11
#define hp20x_PRESSURE_MEASUREMENT			0x30
#define hp20X_ALTITUDE_MEASUREMNET			0x31
#define hp20x_TEMP_MEASUREMENT				0x32

#define hp20x_CONVERSION_REG				0x40
#define hp20x_OSR_CHN_REG					0x48
#define HP203B_ODR_BIT 2
#define	CHNL_PT	 0x00 		//Pressure and temperature channel
#define	CHNL_T   0x02       //Temperature channel
#define	OSR_128	 0x05		//Transfer time, temperature 2.1ms, pressure 4.1ms
#define	OSR_256	 0x04		//Transfer time, temperature 4.1ms, pressure 8.2ms
#define	OSR_512	 0x03		//Transfer time, temperature 8.2ms, pressure 16.4ms
#define	OSR_1024 0x02		//Transfer time, temperature 16.4ms, pressure 32.8ms
	
#define ALT_OFF_LSB							0x00
#define ALT_OFF_MSB							0x01
#define PA_H_TH_LSB							0x02
#define PA_H_TH_MSB							0x03
#define PA_M_TH_LSB							0x04
#define PA_M_TH_MSB							0x05
#define PA_L_TH_LSB							0x06
#define PA_L_TH_MSB							0x07
#define T_H_TH								0x08
#define T_M_TH								0x09
#define T_L_TH								0x0A
#define INT_EN								0x0B
#define INT_CFG								0x0C
#define INT_SRC								0x0D
#define INT_DIR								0x0E
#define PARA								0x0F

#endif
