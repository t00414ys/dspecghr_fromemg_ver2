/*!******************************************************************************
 * @file    bmp280.h
 * @brief   bmp280 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __BMP280_H__
#define __BMP280_H__
#include "bmp280_api.h"
#include "bme280_api.h"



/*
 *
 *		The rest of this is okay not change
 *
 */

#define PRES_BMP_I2C_ADDRESS_L			(0x76)		///< Pressure Sensor(made from BMP) // borch
#define PRES_BMP_I2C_ADDRESS_H			(0x77)		///< Pressure Sensor(made from BMP) // ingenic


#define BMP_WHOAMI_ID					(0x58)
#define BME_WHOAMI_ID					(0x60)
#define BMP_REG_ID						(0xD0)		// Product ID
#define BMP_REG_STATUS					(0xF3)
#define BMP_REG_CTRL_MEAS				(0xF4)
#define BMP_REG_CONFIG					(0xF5)
#define BMP_REG_PRESS					(0xF7)
#define BMP_REG_TEMP					(0xFA)
#define BMP_REG_TRIMMING				(0x88)

#define BME_REG_TRIMMING_H0				(0xA1)
#define BME_REG_TRIMMING_H1				(0xE1)
#define BME_REG_TRIMMING_H2				(0xE2)
#define BME_REG_CRC_REG                 (0xE8)
#define BME_REG_HUMIDITY              	(0xFD)
#define BME_REG_CTRLHUM                 (0xF2)  /*Ctrl Humidity Register*/

#define BMP_PRESS_PER_LSB				(1./(float)(256*100))
#define BMP_TEMP_PER_LSB				(1./(float)100)
#define BME_HUMID_PER_LSB				(1./(float)1024)

// -- CONSTANT //
#define		SLEEP_MODE		0x0
#define		FORCED_MODE		0x1			// one shot
#define		NORMAL_MODE		0x3			// continous

#define		TEMP_OVERSAMPLING_01			(1<<5)// resolution 16bit/0.0050  'C
#define		TEMP_OVERSAMPLING_02			(2<<5)// resolution 17bit/0.0025  'C
#define		TEMP_OVERSAMPLING_04			(3<<5)// resolution 18bit/0.0012  'C
#define		TEMP_OVERSAMPLING_08			(4<<5)// resolution 19bit/0.0006  'C
#define		TEMP_OVERSAMPLING_16			(5<<5)// resolution 20bit/0.0003  'C


#define		PRES_OVERSAMPLING_01			(1<<2)// resolution 16bit/2.87Pa , min.detect.time = 5.5ms
#define		PRES_OVERSAMPLING_02			(2<<2)// resolution 17bit/1.43Pa , min.detect.time = 7.5ms
#define		PRES_OVERSAMPLING_04			(3<<2)// resolution 18bit/0.72Pa , min.detect.time = 11.5ms
#define		PRES_OVERSAMPLING_08			(4<<2)// resolution 19bit/0.36Pa , min.detect.time = 19.5ms
#define		PRES_OVERSAMPLING_16			(5<<2)// resolution 20bit/0.18Pa , min.detect.time = 37.5ms

#define		HUMID_OVERSAMPLING_01			(1)// 
#define		HUMID_OVERSAMPLING_02			(2)// 
#define		HUMID_OVERSAMPLING_04			(3)// 
#define		HUMID_OVERSAMPLING_08			(4)// 
#define		HUMID_OVERSAMPLING_16			(5)// 


#define		IIR_FILTER_0		(0<<2)		// OFF
#define		IIR_FILTER_2		(1<<2)
#define		IIR_FILTER_4		(2<<2)
#define		IIR_FILTER_8		(3<<2)
#define		IIR_FILTER_16		(4<<2)		// highest resolution


// resolution 16bit/0.0050  'C
#endif
