/*!******************************************************************************
 * @file    bma2xx.c
 * @brief   bma2xx sensor driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <string.h>
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "timer.h"
#include "bma2xx.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_BMA2XX

#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(1)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version

typedef	signed char  s8;/**< used for signed 8bit */
typedef	signed short int s16;/**< used for signed 16bit */
typedef	signed long int s32;/**< used for signed 32bit */

struct {
	frizz_fp			scale;		// scaler
	unsigned char		buff[7];	// transmission buffer
	unsigned char		addr;
	unsigned char		device_id;
	unsigned char		ctrl;
	setting_direction_t	setting;

	// ** detect a device error **
	unsigned int		device_condition;
	int					recv_result;
	frizz_fp			lasttime_data[3];
	float				accl_per_lsb;
	//
	unsigned char		v_power_mode;	/**< save current bma2x2 operation mode */
	unsigned char		ctrl_mode_reg;	/**< the value of power mode register 0x11*/
	unsigned char		low_mode_reg;	/**< the value of power mode register 0x12*/
	unsigned char		fifo_config;	/**< store the fifo configuration register*/

} g_bma2xx;


#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char		g_init_done = D_INIT_NONE;

static unsigned char	add_tbl[] = {
	BMA2XX_I2C_ADDRESS_H,
	BMA2XX_I2C_ADDRESS_L,
};

void bma2x2_set_mode_value( unsigned char v_power_mode )
{
	unsigned char data;

	switch( v_power_mode )	{
	case BMA2x2_MODE_NORMAL:	// 130uA
		g_bma2xx.ctrl_mode_reg = ( 0x00 << 5 );
		g_bma2xx.low_mode_reg =	( 0x00 << 5 );
		break;
	case BMA2x2_MODE_LOWPOWER1:
		g_bma2xx.ctrl_mode_reg = ( 0x02 << 5 );
		g_bma2xx.low_mode_reg =	( 0x00 << 5 );
		break;
	case BMA2x2_MODE_LOWPOWER2:	// do not close FIFO, 66uA
		g_bma2xx.ctrl_mode_reg = ( 0x02 << 5 );
		g_bma2xx.low_mode_reg =	( 0x01 << 5 );
		break;
	case BMA2x2_MODE_SUSPEND:	// 2.1uA
		g_bma2xx.ctrl_mode_reg = ( 0x04 << 5 );
		g_bma2xx.low_mode_reg =	( 0x00 << 5 );
		break;
	case BMA2x2_MODE_STANDBY:
		g_bma2xx.ctrl_mode_reg = ( 0x04 << 5 );
		g_bma2xx.low_mode_reg =	( 0x1 << 5 );
		break;
	case BMA2x2_MODE_DEEP_SUSPEND:	// need to do softreset, 1uA
		g_bma2xx.ctrl_mode_reg = ( 0x01 << 5 );
		break;
	}

	data = 0;		// return to normal mode.
	i2c_write( g_bma2xx.addr, BMA2x2_LOW_NOISE_CTRL_REG, &data , 1 );
	mdelay( 1 );
	i2c_write( g_bma2xx.addr, BMA2x2_MODE_CTRL_REG, &data , 1 );
	mdelay( 1 );

	// set low power mode
	data      = g_bma2xx.low_mode_reg;
	data |= BMA2x2_SLEEP_DURN_10MS;
	i2c_write( g_bma2xx.addr, BMA2x2_LOW_NOISE_CTRL_REG, &data , 1 );
	mdelay( 1 );
	// set power mode into suspend
	data      = g_bma2xx.ctrl_mode_reg;
	i2c_write( g_bma2xx.addr, BMA2x2_MODE_CTRL_REG, &data , 1 );
	mdelay( 1 );
}


int bma2xx_init( unsigned int param )
{
	unsigned char	data;
	int				i, ret;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	/* recognition */
	// BMA2XX
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_bma2xx.addr = add_tbl[i];
		g_bma2xx.device_id = 0;
		ret = i2c_read( g_bma2xx.addr, BMA2x2_CHIP_ID_REG, &g_bma2xx.device_id, 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			if( ( g_bma2xx.device_id == BMA2XX_WHOAMI_ID_BMA250E ) || ( g_bma2xx.device_id == BMA2XX_WHOAMI_ID_BMA255 ) || ( g_bma2xx.device_id == BMA2XX_WHOAMI_ID_BMA280 ) ) {
				break;
			}
		}
	}

	if( ( g_bma2xx.device_id != BMA2XX_WHOAMI_ID_BMA250E ) && ( g_bma2xx.device_id != BMA2XX_WHOAMI_ID_BMA255 ) && ( g_bma2xx.device_id != BMA2XX_WHOAMI_ID_BMA280 ) ) {
		return RESULT_ERR_INIT;
	}
	// @@

	if( g_bma2xx.device_id == BMA2XX_WHOAMI_ID_BMA250E ) {
		g_bma2xx.accl_per_lsb = BMA2XX_ACCEL_PER_LSB_BMA250E;
	} else if( g_bma2xx.device_id == BMA2XX_WHOAMI_ID_BMA255 ) {
		g_bma2xx.accl_per_lsb = BMA2XX_ACCEL_PER_LSB_BMA255;
	} else if( g_bma2xx.device_id == BMA2XX_WHOAMI_ID_BMA280 ) {
		g_bma2xx.accl_per_lsb = BMA2XX_ACCEL_PER_LSB_BMA280;
	}

	/* parameter */
	g_bma2xx.scale = as_frizz_fp( g_bma2xx.accl_per_lsb );

	g_bma2xx.ctrl = 0;
	/* setting direction default set */
	if( param == 0 ) {
		g_bma2xx.setting.map_x		= DEFAULT_MAP_X;
		g_bma2xx.setting.map_y		= DEFAULT_MAP_Y;
		g_bma2xx.setting.map_z		= DEFAULT_MAP_Z;
		g_bma2xx.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_bma2xx.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_bma2xx.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
					g_bma2xx.setting.map_x,	g_bma2xx.setting.map_y,	g_bma2xx.setting.map_z,
					g_bma2xx.setting.negate_x, g_bma2xx.setting.negate_y, g_bma2xx.setting.negate_z );
	}

	g_bma2xx.fifo_config      = 0;
	i2c_read( g_bma2xx.addr, BMA2x2_FIFO_MODE_REG, &g_bma2xx.fifo_config , 1 );

	// set range
	data      = BMA2XX_ACC_RANGE;
	i2c_write( g_bma2xx.addr, BMA2x2_RANGE_SELECT_REG, &data , 1 );

	// set bandwidth
	data      = BMA2x2_BW_125HZ;
	i2c_write( g_bma2xx.addr, BMA2x2_BW_SELECT_REG, &data , 1 );

	bma2x2_set_mode_value( BMA2x2_MODE_SUSPEND );

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void bma2xx_ctrl_accl( int f_ena )
{
	if( f_ena == CTRL_ACTIVATE ) {
		bma2x2_set_mode_value( BMA2x2_MODE_LOWPOWER2 );
		//bma2x2_set_mode_value(BMA2x2_MODE_NORMAL);

	} else {
		bma2x2_set_mode_value( BMA2x2_MODE_SUSPEND );
	}

}

unsigned int bma2xx_rcv_accl( unsigned int tick )
{
	g_bma2xx.recv_result = i2c_read( g_bma2xx.addr, BMA2x2_X_AXIS_LSB_REG, &g_bma2xx.buff[0], sizeof( g_bma2xx.buff ) );
	if( g_bma2xx.recv_result == 0 ) {
		g_bma2xx.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_bma2xx.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}
	return 0;
}

int bma2xx_conv_accl( frizz_fp data[3] )
{
	frizz_fp4w			f4w_buff;		// x:0, y:1, z:2
	frizz_fp			*fz = ( frizz_fp* )&f4w_buff;
	float				*fp = ( float* )&f4w_buff;
	sensor_util_half_t	s_buff[4];

	if( g_bma2xx.recv_result != 0 ) {
		data[0] = g_bma2xx.lasttime_data[0];
		data[1] = g_bma2xx.lasttime_data[1];
		data[2] = g_bma2xx.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}

	if( g_bma2xx.device_id == BMA2XX_WHOAMI_ID_BMA250E ) {
		s_buff[0].half = ( ( s16 )( ( ( ( s32 )( ( s8 )g_bma2xx.buff[1] ) ) << 8 ) |	( g_bma2xx.buff[0] & 0xC0 ) ) ) >> 6;
		s_buff[1].half = ( ( s16 )( ( ( ( s32 )( ( s8 )g_bma2xx.buff[3] ) ) << 8 ) |	( g_bma2xx.buff[2] & 0xC0 ) ) ) >> 6;
		s_buff[2].half = ( ( s16 )( ( ( ( s32 )( ( s8 )g_bma2xx.buff[5] ) ) << 8 ) |	( g_bma2xx.buff[4] & 0xC0 ) ) ) >> 6;
	} else if( g_bma2xx.device_id == BMA2XX_WHOAMI_ID_BMA255 ) {
		s_buff[0].half = ( ( s16 )( ( ( ( s32 )( ( s8 )g_bma2xx.buff[1] ) ) << 8 ) |	( g_bma2xx.buff[0] & 0xF0 ) ) ) >> 4;
		s_buff[1].half = ( ( s16 )( ( ( ( s32 )( ( s8 )g_bma2xx.buff[3] ) ) << 8 ) |	( g_bma2xx.buff[2] & 0xF0 ) ) ) >> 4;
		s_buff[2].half = ( ( s16 )( ( ( ( s32 )( ( s8 )g_bma2xx.buff[5] ) ) << 8 ) |	( g_bma2xx.buff[4] & 0xF0 ) ) ) >> 4;
	} else if( g_bma2xx.device_id == BMA2XX_WHOAMI_ID_BMA280 ) {
		s_buff[0].half = ( ( s16 )( ( ( ( s32 )( ( s8 )g_bma2xx.buff[1] ) ) << 8 ) |	( g_bma2xx.buff[0] & 0xFC ) ) ) >> 2;
		s_buff[1].half = ( ( s16 )( ( ( ( s32 )( ( s8 )g_bma2xx.buff[3] ) ) << 8 ) |	( g_bma2xx.buff[2] & 0xFC ) ) ) >> 2;
		s_buff[2].half = ( ( s16 )( ( ( ( s32 )( ( s8 )g_bma2xx.buff[5] ) ) << 8 ) |	( g_bma2xx.buff[4] & 0xFC ) ) ) >> 2;
	}

	// {Dx, Dy, Dz} = {-Sy, -Sx, -Sz}
	fp[0] = ( float )( g_bma2xx.setting.negate_x ? ( -s_buff[g_bma2xx.setting.map_x].half ) : ( s_buff[g_bma2xx.setting.map_x].half ) );
	fp[1] = ( float )( g_bma2xx.setting.negate_y ? ( -s_buff[g_bma2xx.setting.map_y].half ) : ( s_buff[g_bma2xx.setting.map_y].half ) );
	fp[2] = ( float )( g_bma2xx.setting.negate_z ? ( -s_buff[g_bma2xx.setting.map_z].half ) : ( s_buff[g_bma2xx.setting.map_z].half ) );

	f4w_buff = g_bma2xx.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];
	//
	g_bma2xx.lasttime_data[0] = data[0];
	g_bma2xx.lasttime_data[1] = data[1];
	g_bma2xx.lasttime_data[2] = data[2];
	return RESULT_SUCCESS_CONV;
}

int bma2xx_setparam_accl( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_bma2xx.setting.map_x		= setting->map_x;
		g_bma2xx.setting.map_y		= setting->map_y;
		g_bma2xx.setting.map_z		= setting->map_z;
		g_bma2xx.setting.negate_x	= setting->negate_x;
		g_bma2xx.setting.negate_y	= setting->negate_y;
		g_bma2xx.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int bma2xx_accl_get_condition( void *data )
{
	return g_bma2xx.device_condition;
}

int bma2xx_accl_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	if( g_bma2xx.device_id == BMA2XX_WHOAMI_ID_BMA250E ) {
		temp_buffer[0] = ( short )( ( g_bma2xx.buff[1] << 8 ) | ( g_bma2xx.buff[0] & 0xC0 ) );
		temp_buffer[1] = ( short )( ( g_bma2xx.buff[3] << 8 ) | ( g_bma2xx.buff[2] & 0xC0 ) );
		temp_buffer[2] = ( short )( ( g_bma2xx.buff[5] << 8 ) | ( g_bma2xx.buff[4] & 0xC0 ) );
	} else if( g_bma2xx.device_id == BMA2XX_WHOAMI_ID_BMA255 ) {
		temp_buffer[0] = ( short )( ( g_bma2xx.buff[1] << 8 ) | ( g_bma2xx.buff[0] & 0xF0 ) );
		temp_buffer[1] = ( short )( ( g_bma2xx.buff[3] << 8 ) | ( g_bma2xx.buff[2] & 0xF0 ) );
		temp_buffer[2] = ( short )( ( g_bma2xx.buff[5] << 8 ) | ( g_bma2xx.buff[4] & 0xF0 ) );
	} else if( g_bma2xx.device_id == BMA2XX_WHOAMI_ID_BMA280 ) {
		temp_buffer[0] = ( short )( ( g_bma2xx.buff[1] << 8 ) | ( g_bma2xx.buff[0] & 0xFC ) );
		temp_buffer[1] = ( short )( ( g_bma2xx.buff[3] << 8 ) | ( g_bma2xx.buff[2] & 0xFC ) );
		temp_buffer[2] = ( short )( ( g_bma2xx.buff[5] << 8 ) | ( g_bma2xx.buff[4] & 0xFC ) );
	}
	return	RESULT_SUCCESS_SET;
}

unsigned int bma2xx_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int bma2xx_get_name()
{
	return	D_DRIVER_NAME;
}
