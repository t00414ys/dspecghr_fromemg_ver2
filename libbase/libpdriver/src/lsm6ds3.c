/*!******************************************************************************
 * @file    lsm6ds3.c
 * @brief   lsm6ds3 sensor driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <stdio.h>
#include <stdlib.h>

#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "lsm6ds3.h"
#include "config_type.h"


#define		D_DRIVER_NAME			D_DRIVER_NAME_LSM6DS3


#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(1)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version

typedef struct {
	frizz_fp				scale;		// scaler
	unsigned char			buff[6];	// transmission buffer
	setting_direction_t		setting;
	// ** detect a device error **
	unsigned int			device_condition;
	int						recv_result;
	frizz_fp				lasttime_data[3];
	//
} lsm6ds3_accl_sensor_t;

typedef struct {
	frizz_fp4w				scale;		// scaler
	unsigned char			buff[8];	// transmission buffer
	setting_direction_t		setting;
	// ** detect a device error **
	unsigned int			device_condition;
	int						recv_result;
	frizz_fp				lasttime_data[4];
	//
} lsm6ds3_gyro_sensor_t;

struct {
	lsm6ds3_accl_sensor_t	accl;
	lsm6ds3_gyro_sensor_t	gyro;
	unsigned char			pwr_mgmt_accl;
	unsigned char			pwr_mgmt_gyro;
	unsigned char			addr;
	unsigned char			device_id;
} g_lsm6ds3;

#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char		g_init_done = D_INIT_NONE;

static unsigned char	add_tbl[] = {
	LSM6DS3_I2C_ADDRESS_L,
	LSM6DS3_I2C_ADDRESS_H,
};

int lsm6ds3_init( unsigned int param )
{
	int ret, i;

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	// parameter
	// for Accelerometer
	g_lsm6ds3.accl.scale = as_frizz_fp( LSM6DS3_ACCEL_PER_LSB );
	// for Gyroscope
	( ( float* ) & ( g_lsm6ds3.gyro.scale ) )[0] =
		( ( float* ) & ( g_lsm6ds3.gyro.scale ) )[1] =
			( ( float* ) & ( g_lsm6ds3.gyro.scale ) )[2] = LSM6DS3_GYRO_PER_LSB;
	( ( float* ) & ( g_lsm6ds3.gyro.scale ) )[3] = (float)(1.0);
	//((float*)&(g_lsm6ds3.gyro.scale))[3] = LSM6DS3_TEMPERATURE_PER_LSB;

	g_lsm6ds3.pwr_mgmt_accl = 0x00;//acc_power_down;		//
	g_lsm6ds3.pwr_mgmt_gyro = 0x00;//gyro_powerdown;		//

	/* setting direction default set */
	if( param == 0 ) {
		g_lsm6ds3.gyro.setting.map_x		= DEFAULT_MAP_X;
		g_lsm6ds3.gyro.setting.map_y		= DEFAULT_MAP_Y;
		g_lsm6ds3.gyro.setting.map_z		= DEFAULT_MAP_Z;
		g_lsm6ds3.gyro.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_lsm6ds3.gyro.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_lsm6ds3.gyro.setting.negate_z	= SETTING_DIRECTION_ASSERT;
		//
		g_lsm6ds3.accl.setting.map_x		= DEFAULT_MAP_X;
		g_lsm6ds3.accl.setting.map_y		= DEFAULT_MAP_Y;
		g_lsm6ds3.accl.setting.map_z		= DEFAULT_MAP_Z;
		g_lsm6ds3.accl.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_lsm6ds3.accl.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_lsm6ds3.accl.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
					g_lsm6ds3.gyro.setting.map_x,	g_lsm6ds3.gyro.setting.map_y,	g_lsm6ds3.gyro.setting.map_z,
					g_lsm6ds3.gyro.setting.negate_x,	g_lsm6ds3.gyro.setting.negate_y,	g_lsm6ds3.gyro.setting.negate_z );
		EXPAND_MAP(	param,
					g_lsm6ds3.accl.setting.map_x,	g_lsm6ds3.accl.setting.map_y,	g_lsm6ds3.accl.setting.map_z,
					g_lsm6ds3.accl.setting.negate_x,	g_lsm6ds3.accl.setting.negate_y,	g_lsm6ds3.accl.setting.negate_z );
	}

	struct {
		unsigned char	raddr;		// sub address
		//unsigned char	mask;
		unsigned char	data;		// data
	} lsm6ds3_initdata[] = {
		{	LSM6DS3_CTRL3_C,		0x01	}, // SW_RESET
		{	LSM6DS3_CTRL1_XL,		0x00	}, // accel power down
		{	LSM6DS3_CTRL2_G,		0x00	}, // gyro power down
		{	LSM6DS3_CTRL3_C,		0x44	}, // block data update, register address auto increment
		{	LSM6DS3_CTRL6_C,		0x10	}, // disable accel high-performance operating mode
		{	LSM6DS3_CTRL7_C,		0x00	}, // enable gyro high-performance operating mode and disable high-pass filter
		//{	LSM6DS3_CTRL9_XL,		0x38	}, //
		//{	LSM6DS3_CTRL10_C,		0x38	}, // Enable Gyro output, disable other embedded function
		//{	LSM6DS3_FIFO_CTRL2,		0x00	}, // disable pedometer step counter and time stamp
		//{	LSM6DS3_TAP_CFG,		0x00	}, //disable pedometer algorithm
		//{	LSM6DS3_TAP_CFG,		0x81	}, // Re-latch
	};

	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_lsm6ds3.addr =  add_tbl[i];
		g_lsm6ds3.device_id = 0;			// read id
		ret = i2c_read( g_lsm6ds3.addr, LSM6DS3_WHO_AM_I, &g_lsm6ds3.device_id, 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			if( g_lsm6ds3.device_id == LSM6DS3_WHO_AM_I_DEF ) {
				break;
			}
		}
	}

	if( g_lsm6ds3.device_id != LSM6DS3_WHO_AM_I_DEF ) {
		return RESULT_ERR_INIT;
	}
	// @@

	// Initial all reg
	for( i = 0 ; i < ( sizeof( lsm6ds3_initdata ) / sizeof( lsm6ds3_initdata[0] ) ) ; i++ ) {
		ret = i2c_write( g_lsm6ds3.addr, lsm6ds3_initdata[i].raddr, &lsm6ds3_initdata[i].data, 1 );
		if( ret != I2C_RESULT_SUCCESS ) {
			return RESULT_ERR_INIT;
		}
	}

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void lsm6ds3_ctrl_accl( int f_ena )
{
	if( f_ena ) {
		g_lsm6ds3.pwr_mgmt_accl = LSM6DS3_ACC_RANGE | 0x40; //0x44;// 104 hz, 16g
	} else {
		g_lsm6ds3.pwr_mgmt_accl = 0x00;
	}
	i2c_write( g_lsm6ds3.addr, LSM6DS3_CTRL1_XL, &g_lsm6ds3.pwr_mgmt_accl, 1 );
}

void lsm6ds3_ctrl_gyro( int f_ena )
{
	if( f_ena ) {
		g_lsm6ds3.pwr_mgmt_gyro = 0x44; // 104 hx, 500 dps
	} else {
		g_lsm6ds3.pwr_mgmt_gyro = 0x00;
	}
	i2c_write( g_lsm6ds3.addr, LSM6DS3_CTRL2_G, &g_lsm6ds3.pwr_mgmt_gyro, 1 );
}

unsigned int lsm6ds3_rcv_accl( unsigned int tick )
{
	g_lsm6ds3.accl.recv_result = i2c_read( g_lsm6ds3.addr, LSM6DS3_OUTX_L_XL, &g_lsm6ds3.accl.buff[0], sizeof( g_lsm6ds3.accl.buff ) );

	if( g_lsm6ds3.accl.recv_result == 0 ) {
		g_lsm6ds3.accl.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_lsm6ds3.accl.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}

	return 0;
}

unsigned int lsm6ds3_rcv_gyro( unsigned int tick )
{
	g_lsm6ds3.gyro.recv_result = i2c_read( g_lsm6ds3.addr, LSM6DS3_OUT_TEMP_L, &g_lsm6ds3.gyro.buff[0], sizeof( g_lsm6ds3.gyro.buff ) );
	if( g_lsm6ds3.gyro.recv_result == 0 ) {
		g_lsm6ds3.gyro.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_lsm6ds3.gyro.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}

	return 0;
}

int lsm6ds3_conv_accl( frizz_fp data[3] )
{
	frizz_fp4w f4w_buff;		// x:0, y:1, z:2
	frizz_fp* fz = ( frizz_fp* )&f4w_buff;
	float* fp = ( float* )&f4w_buff;
	sensor_util_half_t s_buff[3];	// x:0, y:1, z:2

	if( g_lsm6ds3.accl.recv_result != 0 ) {
		data[0] = g_lsm6ds3.accl.lasttime_data[0];
		data[1] = g_lsm6ds3.accl.lasttime_data[1];
		data[2] = g_lsm6ds3.accl.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}

	/* data for Accel using LSM6DS3 */
	// to half @ little endian
	s_buff[0].ubyte[0] = g_lsm6ds3.accl.buff[0];
	s_buff[0].ubyte[1] = g_lsm6ds3.accl.buff[1];
	s_buff[1].ubyte[0] = g_lsm6ds3.accl.buff[2];
	s_buff[1].ubyte[1] = g_lsm6ds3.accl.buff[3];
	s_buff[2].ubyte[0] = g_lsm6ds3.accl.buff[4];
	s_buff[2].ubyte[1] = g_lsm6ds3.accl.buff[5];
	// to 4way float
	// {Dx, Dy, Dz} = {Sx, Sy, Sz}
	fp[0] = ( float )( g_lsm6ds3.accl.setting.negate_x ? ( -s_buff[g_lsm6ds3.accl.setting.map_x].half ) : ( s_buff[g_lsm6ds3.accl.setting.map_x].half ) );
	fp[1] = ( float )( g_lsm6ds3.accl.setting.negate_y ? ( -s_buff[g_lsm6ds3.accl.setting.map_y].half ) : ( s_buff[g_lsm6ds3.accl.setting.map_y].half ) );
	fp[2] = ( float )( g_lsm6ds3.accl.setting.negate_z ? ( -s_buff[g_lsm6ds3.accl.setting.map_z].half ) : ( s_buff[g_lsm6ds3.accl.setting.map_z].half ) );
	// convert
	f4w_buff = g_lsm6ds3.accl.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];
	//
	g_lsm6ds3.accl.lasttime_data[0] = data[0];
	g_lsm6ds3.accl.lasttime_data[1] = data[1];
	g_lsm6ds3.accl.lasttime_data[2] = data[2];

	return RESULT_SUCCESS_CONV;
}

int lsm6ds3_conv_gyro( frizz_fp data[4] )
{
	frizz_fp4w f4w_buff;		// x:0, y:1, z:2
	frizz_fp* fz = ( frizz_fp* )&f4w_buff;
	float* fp = ( float* )&f4w_buff;
	sensor_util_half_t s_buff[4];	// x:0, y:1, z:2

	if( g_lsm6ds3.gyro.recv_result != 0 ) {
		data[0] = g_lsm6ds3.gyro.lasttime_data[0];
		data[1] = g_lsm6ds3.gyro.lasttime_data[1];
		data[2] = g_lsm6ds3.gyro.lasttime_data[2];
		data[3] = g_lsm6ds3.gyro.lasttime_data[3];
		return RESULT_SUCCESS_CONV;
	}
	/* data for Gyro using LSM6DS3 */
	// to half @ little endian
	s_buff[0].ubyte[0] = g_lsm6ds3.gyro.buff[2];
	s_buff[0].ubyte[1] = g_lsm6ds3.gyro.buff[3];
	s_buff[1].ubyte[0] = g_lsm6ds3.gyro.buff[4];
	s_buff[1].ubyte[1] = g_lsm6ds3.gyro.buff[5];
	s_buff[2].ubyte[0] = g_lsm6ds3.gyro.buff[6];
	s_buff[2].ubyte[1] = g_lsm6ds3.gyro.buff[7];
	s_buff[3].ubyte[0] = g_lsm6ds3.gyro.buff[0];
	s_buff[3].ubyte[1] = g_lsm6ds3.gyro.buff[1];

	// to 4way float
	// {Dx, Dy, Dz} = {Sy, Sx, Sz}
	fp[0] = ( float )( g_lsm6ds3.gyro.setting.negate_x ? ( -s_buff[g_lsm6ds3.gyro.setting.map_x].half ) : ( s_buff[g_lsm6ds3.gyro.setting.map_x].half ) );
	fp[1] = ( float )( g_lsm6ds3.gyro.setting.negate_y ? ( -s_buff[g_lsm6ds3.gyro.setting.map_y].half ) : ( s_buff[g_lsm6ds3.gyro.setting.map_y].half ) );
	fp[2] = ( float )( g_lsm6ds3.gyro.setting.negate_z ? ( -s_buff[g_lsm6ds3.gyro.setting.map_z].half ) : ( s_buff[g_lsm6ds3.gyro.setting.map_z].half ) );
	fp[3] = ( float )( s_buff[3].half );
	// convert
	f4w_buff = g_lsm6ds3.gyro.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];
	data[3] = as_frizz_fp(25.0) + (fz[3]/as_frizz_fp(16.0)); //as_frizz_fp( 0.0 );
	//
	g_lsm6ds3.gyro.lasttime_data[0] = data[0];
	g_lsm6ds3.gyro.lasttime_data[1] = data[1];
	g_lsm6ds3.gyro.lasttime_data[2] = data[2];
	g_lsm6ds3.gyro.lasttime_data[3] = data[3];

	return RESULT_SUCCESS_CONV;
}

int lsm6ds3_setparam_accl( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_lsm6ds3.accl.setting.map_x		= setting->map_x;
		g_lsm6ds3.accl.setting.map_y		= setting->map_y;
		g_lsm6ds3.accl.setting.map_z		= setting->map_z;
		g_lsm6ds3.accl.setting.negate_x	= setting->negate_x;
		g_lsm6ds3.accl.setting.negate_y	= setting->negate_y;
		g_lsm6ds3.accl.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}

	return RESULT_ERR_SET;
}

int lsm6ds3_setparam_gyro( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_lsm6ds3.gyro.setting.map_x		= setting->map_x;
		g_lsm6ds3.gyro.setting.map_y		= setting->map_y;
		g_lsm6ds3.gyro.setting.map_z		= setting->map_z;
		g_lsm6ds3.gyro.setting.negate_x	= setting->negate_x;
		g_lsm6ds3.gyro.setting.negate_y	= setting->negate_y;
		g_lsm6ds3.gyro.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}

	return RESULT_ERR_SET;
}

int lsm6ds3_accl_get_condition( void *data )
{
	return g_lsm6ds3.accl.device_condition;
}


int lsm6ds3_gyro_get_condition( void *data )
{
	return g_lsm6ds3.gyro.device_condition;
}



unsigned int lsm6ds3_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int lsm6ds3_get_name()
{
	return	D_DRIVER_NAME;
}

int lsm6ds3_accl_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	temp_buffer[0] = ( short )( ( g_lsm6ds3.accl.buff[0] << 8 ) | g_lsm6ds3.accl.buff[1] );
	temp_buffer[1] = ( short )( ( g_lsm6ds3.accl.buff[2] << 8 ) | g_lsm6ds3.accl.buff[3] );
	temp_buffer[2] = ( short )( ( g_lsm6ds3.accl.buff[4] << 8 ) | g_lsm6ds3.accl.buff[5] );
	return RESULT_SUCCESS_SET;
}

