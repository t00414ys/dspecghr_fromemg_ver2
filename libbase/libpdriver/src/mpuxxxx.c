/*!******************************************************************************
 * @file    mpuxxxx.c
 * @brief   mpuxxxx sensor driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "sensor_util.h"
#include "hub_util.h"
#include "base_driver.h"
#include "frizz_peri.h"
#include "i2c.h"
#include "timer.h"
#include "mpuxxxx.h"
#include "config_type.h"

#define		D_DRIVER_NAME			D_DRIVER_NAME_MPUXXXX


#define		DRIVER_VER_MAJOR		(1)						// Major Version
#define		DRIVER_VER_MINOR		(1)						// Minor Version
#define		DRIVER_VER_DETAIL		(0)						// Detail Version

#if MPUXXXX_FULLCHIP_SLEEP
mpuxxxx_ctrl_t mpuxxxx_ctrl;      // check MPU-xxxx is on or in not   0:off,1:on
#endif

typedef struct {
	frizz_fp				scale;		// scaler
	unsigned char			buff[6];	// transmission buffer
	setting_direction_t		setting;
	// ** detect a device error **
	unsigned int			device_condition;
	int						ena;
	int						recv_result;
	frizz_fp				lasttime_data[3];
	//
} mpuxxxx_accl_sensor_t;

typedef struct {
	frizz_fp4w				scale;		// scaler
	unsigned char			buff[8];	// transmission buffer
	setting_direction_t		setting;
	// ** detect a device error **
	unsigned int			device_condition;
	int						ena;
	int						recv_result;
	frizz_fp				lasttime_data[4];
	//
} mpuxxxx_gyro_sensor_t;

struct {
	mpuxxxx_accl_sensor_t	accl;
	mpuxxxx_gyro_sensor_t	gyro;
	unsigned char			pwr_mgmt[2];
	unsigned char 			device_id;
	unsigned char			addr;
} g_mpuxxxx;


#define	D_INIT_NONE		(0)
#define	D_INIT_DONE		(1)
static unsigned char		g_init_done = D_INIT_NONE;

static unsigned char	add_tbl[] = {
	MPUXXXX_I2C_ADDRESS_H,
	MPUXXXX_I2C_ADDRESS_L,
};

int mpuxxxx_set_chip_reset( void )
{
	int 				ret;
	unsigned char		data;
	data   = 0x80;			// RESET:
	ret = i2c_write( g_mpuxxxx.addr, MPUXXXX_PWR_MGMT_1, &data, 1 );

	mdelay( 20 );
	return ret;
}

#if MPUXXXX_ACCL_LOWPOWER
// set MPU9255 accl low power mode
void mpuxxxx_accl_lowpower( int set )
{
	unsigned char	data;

	if( set != 0 ) {
		data = 0x08;			// FCHOICE_B:1, DLPFCFG:0
		i2c_write( g_mpuxxxx.addr, MPUXXXX_ACCEL_CONFIG2, &data, 1 ); //reg29

		data = 0x09;			// CLKSEL 2:0.98Hz, 7:31.25Hz, 9:125Hz
		i2c_write( g_mpuxxxx.addr, MPUXXXX_LP_ACCEL_ODR, &data, 1 ); //reg30

		g_mpuxxxx.pwr_mgmt[0] = g_mpuxxxx.pwr_mgmt[0] | 0x20;			// CYCLE:1
		i2c_write( g_mpuxxxx.addr, MPUXXXX_PWR_MGMT_1, &g_mpuxxxx.pwr_mgmt[0], 1 ); //reg107
	} else {
		data = 0x00;			// FCHOICE_B:1, DLPFCFG:0
		i2c_write( g_mpuxxxx.addr, MPUXXXX_ACCEL_CONFIG2, &data, 1 ); //reg29

		data = 0x00;			// CLKSEL 2:0.98Hz, 7:31.25Hz, 9:125Hz
		i2c_write( g_mpuxxxx.addr, MPUXXXX_LP_ACCEL_ODR, &data, 1 ); //reg30

		g_mpuxxxx.pwr_mgmt[0] = g_mpuxxxx.pwr_mgmt[0] & ~0x20;			// CYCLE:0
		i2c_write( g_mpuxxxx.addr, MPUXXXX_PWR_MGMT_1, &g_mpuxxxx.pwr_mgmt[0], 1 ); //reg107
	}
}
#endif

#if MPUXXXX_FULLCHIP_SLEEP
void mpuxxxx_set_chip_wake( void )
{
	unsigned char	buff;
	unsigned char	data;

	buff      = 0;
	i2c_read( g_mpuxxxx.addr, MPUXXXX_PWR_MGMT_1, &buff, 1 );

	data = buff & 0xBF;
	i2c_write( g_mpuxxxx.addr, MPUXXXX_PWR_MGMT_1, &data, 1 );
}

void mpuxxxx_set_chip_sleep( void )
{
	unsigned char		buff;
	unsigned char		data;

	mpuxxxx_ctrl.data = 0;      // MPU-xxxx off

	buff      = 0;
	i2c_read( g_mpuxxxx.addr, MPUXXXX_PWR_MGMT_1, &buff, 1 );

	// setting for MPU-xxxx sleep mode
	data = buff | 0x40;
	i2c_write( g_mpuxxxx.addr, MPUXXXX_PWR_MGMT_1, &data, 1 );
}
#endif   //end of MPUXXXX_FULLCHIP_SLEEP

int mpuxxxx_init( unsigned int param )
{
	int					ret, i;
	unsigned char		data[5];

#if MPUXXXX_FULLCHIP_SLEEP
	mpuxxxx_ctrl.data = 0;      // MPU-xxxx off
#endif

	if( g_init_done != D_INIT_NONE ) {
		return RESULT_SUCCESS_INIT;
	}

	/* parameter */
	// for Accelerometer
	g_mpuxxxx.accl.scale = as_frizz_fp( MPUXXXX_ACCEL_PER_LSB );
	// for Gyroscope
	( ( float* ) & ( g_mpuxxxx.gyro.scale ) )[0] =
		( ( float* ) & ( g_mpuxxxx.gyro.scale ) )[1] =
			( ( float* ) & ( g_mpuxxxx.gyro.scale ) )[2] = MPUXXXX_GYRO_PER_LSB;
	( ( float* ) & ( g_mpuxxxx.gyro.scale ) )[3] = MPUXXXX_TEMPERATURE_PER_LSB;
	// PWR_MGMT_1		DEVICE_RESET:0, SLEEP:0, CYCLE:0, GYRO_STANDBY:0, TEMP_DIS:1, CLKSEL:1
	g_mpuxxxx.pwr_mgmt[0] = 0x09;
	// PWR_MGMT_2		LP_WAKE_CTRL[7:6]:0, DISABLE_?A[5:3]:7, DISABLE_?G[2:0]:7
	g_mpuxxxx.pwr_mgmt[1] = 0x3F;

	g_mpuxxxx.accl.ena = 0;
	g_mpuxxxx.gyro.ena = 0;

	/* setting direction default set */

	if( param == 0 ) {
		g_mpuxxxx.gyro.setting.map_x	= DEFAULT_MAP_X;
		g_mpuxxxx.gyro.setting.map_y	= DEFAULT_MAP_Y;
		g_mpuxxxx.gyro.setting.map_z	= DEFAULT_MAP_Z;
		g_mpuxxxx.gyro.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_mpuxxxx.gyro.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_mpuxxxx.gyro.setting.negate_z	= SETTING_DIRECTION_ASSERT;
		//
		g_mpuxxxx.accl.setting.map_x	= DEFAULT_MAP_X;
		g_mpuxxxx.accl.setting.map_y	= DEFAULT_MAP_Y;
		g_mpuxxxx.accl.setting.map_z	= DEFAULT_MAP_Z;
		g_mpuxxxx.accl.setting.negate_x	= SETTING_DIRECTION_ASSERT;
		g_mpuxxxx.accl.setting.negate_y	= SETTING_DIRECTION_ASSERT;
		g_mpuxxxx.accl.setting.negate_z	= SETTING_DIRECTION_ASSERT;
	} else {
		EXPAND_MAP(	param,
					g_mpuxxxx.gyro.setting.map_x,	g_mpuxxxx.gyro.setting.map_y,	g_mpuxxxx.gyro.setting.map_z,
					g_mpuxxxx.gyro.setting.negate_x, g_mpuxxxx.gyro.setting.negate_y, g_mpuxxxx.gyro.setting.negate_z );
		EXPAND_MAP(	param,
					g_mpuxxxx.accl.setting.map_x,	g_mpuxxxx.accl.setting.map_y,	g_mpuxxxx.accl.setting.map_z,
					g_mpuxxxx.accl.setting.negate_x, g_mpuxxxx.accl.setting.negate_y, g_mpuxxxx.accl.setting.negate_z );
	}

	/* recognition */
	// MPU-xxxx
	// @@ i2c address search
	for( i = 0 ; i < NELEMENT( add_tbl ) ; i++ ) {
		g_mpuxxxx.addr =  add_tbl[i];
		g_mpuxxxx.device_id = 0;			// read id
		ret = i2c_read( g_mpuxxxx.addr, MPUXXXX_WHO_AM_I, &g_mpuxxxx.device_id, 1 );
		if( ret == I2C_RESULT_SUCCESS ) {
			if( ( g_mpuxxxx.device_id == MPUXXXX_WHOAMI_ID_MPU6505_6500 ) || ( g_mpuxxxx.device_id == MPUXXXX_WHOAMI_ID_MPU9255 ) || ( g_mpuxxxx.device_id == MPUXXXX_WHOAMI_ID_MPU9250 ) ) {
				break;
			}
		}
	}

	if( ( g_mpuxxxx.device_id != MPUXXXX_WHOAMI_ID_MPU6505_6500 ) && ( g_mpuxxxx.device_id != MPUXXXX_WHOAMI_ID_MPU9255 ) && ( g_mpuxxxx.device_id != MPUXXXX_WHOAMI_ID_MPU9250 ) ) {
		return RESULT_ERR_INIT;
	}
	// @@


#if MPUXXXX_ACCL_LOWPOWER
	ret = mpuxxxx_set_chip_reset();
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}
#endif

	/* setting part 1*/
	data[0]   = 0x09;			// SMPLRT_DIV		sampling rate (Hz) = 1KHz(for MPU9150)/(1+SMPLRT_DIV)
	data[1]   = 0x04;			// CONFIG			FIFO: rplace oldest, EXT_SYNC_SET:disable, DLPF_CFG: 4
	data[2]   = 0x10;			// GYRO_CONFIG		XG_ST:none, YG_ST:none, ZG_ST:none, GYRO_FS_SEL:10, FCHOICE_B:00
	// FS_SEL: 00:+-250dps, 01:+-500dps, 10=+-1000dps, 11=+-2000dps
	data[3]   = MPUXXXX_ACC_RANGE;			// ACCEL_CONFIG		XA_ST:none, YA_ST:none, ZA_ST:none, ACCEL_FS_SEL:01
	data[4]   = 0x02;			// ACCEL_CONFIG2	ACCEL_FCHOICE_B:0, A_DLPF_CFG:
	ret = i2c_write( g_mpuxxxx.addr, MPUXXXX_SMPLRT_DIV, &data[0], 5 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	/* setting part 2 */
	data[0]   = g_mpuxxxx.pwr_mgmt[0];			// PWR_MGMT_1		DEVICE_RESET:0, SLEEP:0, CYCLE:0, GYRO_STANDBY:0, TEMP_DIS:0, CLKSEL:1
	data[1]   = g_mpuxxxx.pwr_mgmt[1];			// PWR_MGMT_2		LP_WAKE_CTRL[7:6]:0, DISABLE_A[5:3]:7, DISABLE_G[2:0]:7
	ret = i2c_write( g_mpuxxxx.addr, MPUXXXX_PWR_MGMT_1, &data[0], 2 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

	/* setting part 3 */
	data[0]   = 0xC0;			// INT_PIN_CFG		[6] : 1 - INT pin is configured as open drain.
	ret = i2c_write( g_mpuxxxx.addr, MPUXXXX_INT_PIN_CFG, &data[0], 1 );
	if( ret != I2C_RESULT_SUCCESS ) {
		return RESULT_ERR_INIT;
	}

#if MPUXXXX_FULLCHIP_SLEEP
	/* setting mpuxxxx into sleep mode */
	mpuxxxx_set_chip_sleep();
#endif

	g_init_done = D_INIT_DONE;
	return RESULT_SUCCESS_INIT;
}

void mpuxxxx_ctrl_accl( int f_ena )
{
#if MPUXXXX_FULLCHIP_SLEEP
	mpuxxxx_set_chip_wake();
#endif
	if( f_ena ) {
		g_mpuxxxx.pwr_mgmt[1] &= 0xC7;

#if MPUXXXX_FULLCHIP_SLEEP
		mpuxxxx_ctrl.data |= 0x01;      // MPU-xxxx on
#endif
	} else {
		g_mpuxxxx.pwr_mgmt[1] |= 0x38;

#if MPUXXXX_FULLCHIP_SLEEP
		mpuxxxx_ctrl.data &= ~0x01;      // MPU-xxxx off
#endif
	}
	i2c_write( g_mpuxxxx.addr, MPUXXXX_PWR_MGMT_2, &g_mpuxxxx.pwr_mgmt[1], 1 );
	g_mpuxxxx.accl.ena = f_ena;

#if MPUXXXX_ACCL_LOWPOWER
	if( ( g_mpuxxxx.accl.ena != 0 ) && ( g_mpuxxxx.gyro.ena == 0 ) ) {
		// set MPUxxxx accl low power mode
		mpuxxxx_accl_lowpower( 1 );
	} else if( g_mpuxxxx.gyro.ena != 0 ) {
		// reset MPUxxxx accl low power mode
		mpuxxxx_accl_lowpower( 0 );
	}
#endif
#if MPUXXXX_FULLCHIP_SLEEP
	// set MPUxxxx sleep mode
	if( mpuxxxx_ctrl.data == 0 ) {
		mpuxxxx_set_chip_sleep();
	}
#endif
}

void mpuxxxx_ctrl_gyro( int f_ena )
{
#if MPUXXXX_FULLCHIP_SLEEP
	mpuxxxx_set_chip_wake();
#endif

	if( f_ena ) {
		g_mpuxxxx.pwr_mgmt[0] &= 0xF7;
		g_mpuxxxx.pwr_mgmt[1] &= 0xF8;

#if MPUXXXX_FULLCHIP_SLEEP
		mpuxxxx_ctrl.data |= 0x04;
#endif
	} else {
		g_mpuxxxx.pwr_mgmt[0] |= 0x08;
		g_mpuxxxx.pwr_mgmt[1] |= 0x07;

#if MPUXXXX_FULLCHIP_SLEEP
		mpuxxxx_ctrl.data &= ~0x04;
#endif
	}
	i2c_write( g_mpuxxxx.addr, MPUXXXX_PWR_MGMT_1, &g_mpuxxxx.pwr_mgmt[0], 2 );

	g_mpuxxxx.gyro.ena = f_ena;

#if MPUXXXX_ACCL_LOWPOWER
	if( ( g_mpuxxxx.accl.ena != 0 ) && ( g_mpuxxxx.gyro.ena == 0 ) ) {
		// set MPUxxxx accl low power mode
		mpuxxxx_accl_lowpower( 1 );
	} else if( g_mpuxxxx.gyro.ena != 0 ) {
		// reset MPUxxxx accl low power mode
		mpuxxxx_accl_lowpower( 0 );
	}
#endif
#if MPUXXXX_FULLCHIP_SLEEP
	// set MPUxxxx sleep mode
	if( mpuxxxx_ctrl.data == 0 ) {
		mpuxxxx_set_chip_sleep();
	}
#endif
}

unsigned int mpuxxxx_rcv_accl( unsigned int tick )
{
	g_mpuxxxx.accl.recv_result = i2c_read( g_mpuxxxx.addr, MPUXXXX_ACCEL_XOUT_H, &g_mpuxxxx.accl.buff[0], 6 );
	if( g_mpuxxxx.accl.recv_result == 0 ) {
		g_mpuxxxx.accl.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_mpuxxxx.accl.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}
	return 0;
}

int mpuxxxx_conv_accl( frizz_fp data[3] )
{
	frizz_fp4w			f4w_buff;					// x:0, y:1, z:2
	frizz_fp			*fz = ( frizz_fp* )&f4w_buff;
	float				*fp = ( float* )&f4w_buff;
	sensor_util_half_t	s_buff[3];					// x:0, y:1, z:2

	if( g_mpuxxxx.accl.recv_result != 0 ) {
		data[0] = g_mpuxxxx.accl.lasttime_data[0];
		data[1] = g_mpuxxxx.accl.lasttime_data[1];
		data[2] = g_mpuxxxx.accl.lasttime_data[2];
		return RESULT_SUCCESS_CONV;
	}

	/* data for Accel using MPU-xxxx */
	// to half @ little endian
	s_buff[0].ubyte[1] = g_mpuxxxx.accl.buff[0];
	s_buff[0].ubyte[0] = g_mpuxxxx.accl.buff[1];
	s_buff[1].ubyte[1] = g_mpuxxxx.accl.buff[2];
	s_buff[1].ubyte[0] = g_mpuxxxx.accl.buff[3];
	s_buff[2].ubyte[1] = g_mpuxxxx.accl.buff[4];
	s_buff[2].ubyte[0] = g_mpuxxxx.accl.buff[5];
	// to 4way float
	// {Dx, Dy, Dz} = {Sy, -Sx, Sz}
	fp[0] = ( float )( g_mpuxxxx.accl.setting.negate_x ? ( -s_buff[g_mpuxxxx.accl.setting.map_x].half ) : ( s_buff[g_mpuxxxx.accl.setting.map_x].half ) );
	fp[1] = ( float )( g_mpuxxxx.accl.setting.negate_y ? ( -s_buff[g_mpuxxxx.accl.setting.map_y].half ) : ( s_buff[g_mpuxxxx.accl.setting.map_y].half ) );
	fp[2] = ( float )( g_mpuxxxx.accl.setting.negate_z ? ( -s_buff[g_mpuxxxx.accl.setting.map_z].half ) : ( s_buff[g_mpuxxxx.accl.setting.map_z].half ) );
	// convert
	f4w_buff = g_mpuxxxx.accl.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];
	//
	g_mpuxxxx.accl.lasttime_data[0] = data[0];
	g_mpuxxxx.accl.lasttime_data[1] = data[1];
	g_mpuxxxx.accl.lasttime_data[2] = data[2];
	return RESULT_SUCCESS_CONV;
}

/* reading gyroscope */
unsigned int mpuxxxx_rcv_gyro( unsigned int tick )
{
	g_mpuxxxx.gyro.recv_result = i2c_read( g_mpuxxxx.addr, MPUXXXX_TEMP_OUT_H, &g_mpuxxxx.gyro.buff[0], 8 );
	if( g_mpuxxxx.gyro.recv_result == 0 ) {
		g_mpuxxxx.gyro.device_condition  &= ( ~D_RAW_DEVICE_ERR_READ );
	} else {
		g_mpuxxxx.gyro.device_condition  |= D_RAW_DEVICE_ERR_READ;
	}
	return 0;
}

int mpuxxxx_conv_gyro( frizz_fp data[4] )
{
	frizz_fp4w			f4w_buff;					// x:0, y:1, z:2
	frizz_fp			*fz = ( frizz_fp* )&f4w_buff;
	float				*fp = ( float* )&f4w_buff;
	sensor_util_half_t	s_buff[4];					// x:0, y:1, z:2, t:3

	if( g_mpuxxxx.gyro.recv_result != 0 ) {
		data[0] = g_mpuxxxx.gyro.lasttime_data[0];
		data[1] = g_mpuxxxx.gyro.lasttime_data[1];
		data[2] = g_mpuxxxx.gyro.lasttime_data[2];
		data[3] = g_mpuxxxx.gyro.lasttime_data[3];
		return RESULT_SUCCESS_CONV;
	}
	/* data for Gyro using MPU-xxxx */
	// to half @ little endian
	s_buff[0].ubyte[1] = g_mpuxxxx.gyro.buff[2];
	s_buff[0].ubyte[0] = g_mpuxxxx.gyro.buff[3];
	s_buff[1].ubyte[1] = g_mpuxxxx.gyro.buff[4];
	s_buff[1].ubyte[0] = g_mpuxxxx.gyro.buff[5];
	s_buff[2].ubyte[1] = g_mpuxxxx.gyro.buff[6];
	s_buff[2].ubyte[0] = g_mpuxxxx.gyro.buff[7];
	s_buff[3].ubyte[1] = g_mpuxxxx.gyro.buff[0];
	s_buff[3].ubyte[0] = g_mpuxxxx.gyro.buff[1];
	// to 4way float
	// {Dx, Dy, Dz} = {Sy, -Sx, Sz}
	fp[0] = ( float )( g_mpuxxxx.gyro.setting.negate_x ? ( -s_buff[g_mpuxxxx.gyro.setting.map_x].half ) : ( s_buff[g_mpuxxxx.gyro.setting.map_x].half ) );
	fp[1] = ( float )( g_mpuxxxx.gyro.setting.negate_y ? ( -s_buff[g_mpuxxxx.gyro.setting.map_y].half ) : ( s_buff[g_mpuxxxx.gyro.setting.map_y].half ) );
	fp[2] = ( float )( g_mpuxxxx.gyro.setting.negate_z ? ( -s_buff[g_mpuxxxx.gyro.setting.map_z].half ) : ( s_buff[g_mpuxxxx.gyro.setting.map_z].half ) );
	fp[3] = ( float )( s_buff[3].half );
	// convert
	f4w_buff = g_mpuxxxx.gyro.scale * f4w_buff;
	data[0] = fz[0];
	data[1] = fz[1];
	data[2] = fz[2];
	data[3] = fz[3] + as_frizz_fp( 25.0 );
	//
	g_mpuxxxx.gyro.lasttime_data[0] = data[0];
	g_mpuxxxx.gyro.lasttime_data[1] = data[1];
	g_mpuxxxx.gyro.lasttime_data[2] = data[2];
	g_mpuxxxx.gyro.lasttime_data[3] = data[3];
	return RESULT_SUCCESS_CONV;
}


int mpuxxxx_setparam_accl( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_mpuxxxx.accl.setting.map_x	= setting->map_x;
		g_mpuxxxx.accl.setting.map_y	= setting->map_y;
		g_mpuxxxx.accl.setting.map_z	= setting->map_z;
		g_mpuxxxx.accl.setting.negate_x	= setting->negate_x;
		g_mpuxxxx.accl.setting.negate_y	= setting->negate_y;
		g_mpuxxxx.accl.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int mpuxxxx_setparam_gyro( void *ptr )
{
	setting_direction_t	*setting = ( setting_direction_t	* ) ptr;

	if( ( setting->map_x != setting->map_y ) && ( setting->map_x != setting->map_z ) &&
		( setting->map_x <=  DEFAULT_MAP_Z ) && ( setting->map_y <=  DEFAULT_MAP_Z ) && ( setting->map_z <=  DEFAULT_MAP_Z ) ) {
		g_mpuxxxx.gyro.setting.map_x	= setting->map_x;
		g_mpuxxxx.gyro.setting.map_y	= setting->map_y;
		g_mpuxxxx.gyro.setting.map_z	= setting->map_z;
		g_mpuxxxx.gyro.setting.negate_x	= setting->negate_x;
		g_mpuxxxx.gyro.setting.negate_y	= setting->negate_y;
		g_mpuxxxx.gyro.setting.negate_z	= setting->negate_z;
		return RESULT_SUCCESS_SET;
	}
	return RESULT_ERR_SET;
}

int mpuxxxx_accl_get_condition( void *data )
{
	return g_mpuxxxx.accl.device_condition;
}

int mpuxxxx_gyro_get_condition( void *data )
{
	return g_mpuxxxx.gyro.device_condition;
}

int mpuxxxx_accl_get_raw_data( void *data )
{
	short *temp_buffer = ( short* )data;
	temp_buffer[0] = ( short )( ( g_mpuxxxx.accl.buff[0] << 8 ) | g_mpuxxxx.accl.buff[1] );
	temp_buffer[1] = ( short )( ( g_mpuxxxx.accl.buff[2] << 8 ) | g_mpuxxxx.accl.buff[3] );
	temp_buffer[2] = ( short )( ( g_mpuxxxx.accl.buff[4] << 8 ) | g_mpuxxxx.accl.buff[5] );

	return	RESULT_SUCCESS_SET;
}

unsigned int mpuxxxx_get_ver()
{
	return	make_version( DRIVER_VER_MAJOR, DRIVER_VER_MINOR, DRIVER_VER_DETAIL );
}

unsigned int mpuxxxx_get_name()
{
	return	D_DRIVER_NAME;
}

unsigned char mpuxxxx_get_id()
{
	return g_mpuxxxx.device_id;
}

unsigned char mpuxxxx_get_addr()
{
	return g_mpuxxxx.addr;
}

