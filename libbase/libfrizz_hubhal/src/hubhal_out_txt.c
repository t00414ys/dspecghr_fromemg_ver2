/*!******************************************************************************
 * @file hubhal_out_txt.c
 * @brief source code for hubhal_out_txt
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <xtensa/xtruntime.h>
#include "frizz_peri.h"
#include "frizz_env.h"
#include "frizz_util.h"
#include "quart.h"
#include "hubhal_out_txt.h"
#include "hubhal_format.h"
#include "libsensors_id.h"
#include "if/bp_sen_if.h"



/**
 * @brief initialize
 *
 * @param freq UART block clock frequency
 * @param baudrate target baudrate
 */
void hubhal_out_txt_init( unsigned int freq, QUART_BAUDRATE_MODE baudrate )
{
	quart_init( freq, baudrate, 1 );
}

/**
 * @brief send output data
 *
 * @param sen_id sensor ID
 * @param area sensor area (0 or 1)
 * @param ts timestamp
 * @param num number of data
 * @param data data
 * @param f_block blocking flag(0:disable, !0:enable)
 * @param f_notify interrupt signal flag(0:disable, 1:enable)
 *
 * @return 0: Success, <0: Fail
 */
int hubhal_out_txt_snd_data( unsigned char sen_id, unsigned int area, unsigned int ts, unsigned int num, void *data, int f_block, int f_notify )
{
	unsigned int i;

	_xtos_ints_off( 1 << INTERRUPT_NO_MESSAGE );
	// transaction--->
	quart_out_raw( "[snd_data][%08d][%d]\tsen_id:0x%02x", ts, area, sen_id );
	if( sen_id == 0xFF ) {
		int* val = ( int* )data;
		for( i = 0; i < num; i++ ) {
			quart_out_raw( ", val[%d]:0x%08x", i, val[i] );
		}
	} else if( sen_id == SENSOR_ID_ACCEL_PEDOMETER ) {	// accl pedo
		int* val = ( int* )data;
		for( i = 0; i < num; i++ ) {
			quart_out_raw( ", val[%d]:%d", i, val[i] );
		}
	} else if( sen_id == SENSOR_ID_ACTIVITY_DETECTOR ) {	// activity
		int* val = ( int* )data;
		for( i = 0; i < num; i++ ) {
			quart_out_raw( ", val[%d]:%d", i, val[i] );
		}
	} else if( sen_id == SENSOR_ID_MOTION_DETECTOR ) {	// motion detect
		int* val = ( int* )data;
		for( i = 0; i < num; i++ ) {
			quart_out_raw( ", val[%d]:%d", i, val[i] );
		}
	} else if( sen_id == SENSOR_ID_MOTION_SENSING ) {	// motion sensing
		int* val = ( int* )data;
		for( i = 0; i < num; i++ ) {
			quart_out_raw( ", val[%d]:%d", i, val[i] );
		}
	} else if( sen_id == SENSOR_ID_GESTURE ) {	// gesture
		int* val = ( int* )data;
		for( i = 0; i < num; i++ ) {
			quart_out_raw( ", val[%d]:%d", i, val[i] );
		}
	} else if( sen_id == SENSOR_ID_BLOOD_PRESSURE ) {		// blood pressure
		ST_BLOOD_PRESSURE_DATA* val = ( ST_BLOOD_PRESSURE_DATA* )data;

		quart_out_raw( ", hr:%d",		val->HRF );
		quart_out_raw( ", bp_max:%d",	val->BP_MAX );
		quart_out_raw( ", bp_min:%d",	val->BP_MIN );
		quart_out_raw( ", ht(tm):%d",	val->SAMPLE_HR );
		quart_out_raw( ", Cal:%f",		val->BnCal );
		quart_out_raw( ", status:%d",	val->status );
	} else if( sen_id == SENSOR_ID_BLOOD_PRESSURE_LEARN ) {	// blood pressure learn
		;
	} else {
		float* val = ( float* )data;
		for( i = 0; i < num; i++ ) {
			quart_out_raw( ", val[%d]:%f", i, val[i] );
		}
	}
	quart_out_raw( ", f_block:%d", f_block );
	quart_out_raw( ", f_notify:%d", f_notify );
	quart_out_raw( "\r\n" );
	// <---transaction
	_xtos_ints_on( 1 << INTERRUPT_NO_MESSAGE );

	return 0;
}

/**
 * @brief output response for command
 *
 * @param sen_id sensor ID
 * @param cmd_code CMD Code
 * @param res Response Code
 */
void hubhal_out_txt_res_cmd( unsigned char sen_id, unsigned int cmd_code, int res, unsigned int num, void *data )
{
	_xtos_ints_off( 1 << INTERRUPT_NO_MESSAGE );
	// transaction--->
	quart_out_raw( "[res_cmd]\tsen_id:0x%02x", sen_id );
	if( num == 0 ) {
		quart_out_raw( ", cmd_code:0x%08x, res:0x%08x", cmd_code, res );
	} else {
		int				i;
		unsigned int	*ptr = ( unsigned int * ) data;
		quart_out_raw( ", cmd_code:0x%08x, res:", cmd_code );
		for( i = 0 ; i < num ; i++ ) {
			quart_out_raw( ",0x%08x", *ptr );
			ptr++;
		}
	}
	quart_out_raw( "\r\n" );
	// <---transaction
	_xtos_ints_on( 1 << INTERRUPT_NO_MESSAGE );
}

/**
 * @brief ACK
 *
 * @param f_ack 0:NACK, !0:ACK
 */
void hubhal_out_txt_ack( int f_ack, unsigned int mes_uart )
{
	_xtos_ints_off( 1 << INTERRUPT_NO_MESSAGE );
	// transaction--->
	if( f_ack ) {
		quart_out_raw( "[U_get]=[0x%08X]\t[ack]=[0x%X]", mes_uart, HUBHAL_FORMAT_ACK_CODE );
	} else {
		quart_out_raw( "[U_get]=[0x%08X]\t[ack]=[0x%X]", mes_uart, HUBHAL_FORMAT_NACK_CODE );
	}
	quart_out_raw( "\r\n" );
	// <---transaction
	_xtos_ints_on( 1 << INTERRUPT_NO_MESSAGE );
}

/**
 * @brief output
 *
 * @param [in] int_no 0~3:with falling edge signal of specific GPIO no at event, <0:without interrupt signal
 */
void hubhal_out_txt_set_int_no( int int_no, int level )
{
}
