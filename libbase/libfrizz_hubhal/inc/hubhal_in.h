/*!******************************************************************************
 * @file hubhal_in.h
 * @brief source code for hubhal_in.c
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HUBHAL_IN_H__
#define __HUBHAL_IN_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief input path
 */
typedef enum {
	HUBHAL_IN_FROM_MES = 0,	///< use message register
	HUBHAL_IN_FROM_QUEUE	///< use input queue for DEBUG
} HUBHAL_IN_SOURCE;

/**
 * @brief Init
 *
 * @param [in] input type #HUBHAL_IN_SOURCE
 */
void hubhal_in_init( HUBHAL_IN_SOURCE type );

/**
 * @brief get command
 *
 * @param [out] sen_id sensor ID
 * @param [out] cmd_code CMD Code
 * @param [out] num amount of parameters
 * @param [out] params pointer to parameters buffer
 *
 * @return 0: no command, !0: got command
 */
int hubhal_in_get_cmd( unsigned char *sen_id, unsigned int *cmd_code, unsigned int *num, void **params );

#ifdef __cplusplus
}
#endif

#endif//__HUBHAL_IN_H__
