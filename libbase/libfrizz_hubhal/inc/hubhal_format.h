/*!******************************************************************************
 * @file hubhal_format.h
 * @brief source code for hubhal_format.c
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __HUBHAL_FORMAT_H__
#define __HUBHAL_FORMAT_H__

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief Header
 */
typedef union {
	unsigned int	w;
	struct {
		unsigned char	num;		///< payload word num
		unsigned char	sen_id;		///< sensor ID
		unsigned char	type;		///< 0x80: SensorOutput, 0x81: Command, 0x82: MessageACK, 0x83: MessageNACK, 0x84: Response, 0x8F: BreakCode
		unsigned char	prefix;		///< 0xFF
	};
} hubhal_format_header_t;

/**
 * @brief ack code
 * @note ACK from frizz side
 */
#define HUBHAL_FORMAT_ACK_CODE		0xFF82FF00

/**
 * @brief nack code
 * @note NACK from frizz side
 */
#define HUBHAL_FORMAT_NACK_CODE		0xFF83FF00

/**
 * @brief break code
 * @note clear command setting state
 */
#define HUBHAL_FORMAT_BREAK_CODE	0xFF8FFF00

/**
 * @brief prefix code
 */
#define HUBHAL_FORMAT_PREFIX		0xFF

/**
 * @brief type code for sensor data
 */
#define HUBHAL_FORMAT_TYPE_SENSOR_DATA					0x80

/**
 * @brief type code for command code
 */
#define HUBHAL_FORMAT_TYPE_COMMAND						0x81

/**
 * @brief type code for ACK
 */
#define HUBHAL_FORMAT_TYPE_ACK							0x82

/**
 * @brief type code for NACK
 */
#define HUBHAL_FORMAT_TYPE_NACK							0x83

/**
 * @brief type code for response
 */
#define HUBHAL_FORMAT_TYPE_RESPONSE						0x84

/**
 * @brief type code for sensor data as secondary
 */
#define HUBHAL_FORMAT_TYPE_SENSOR_DATA_SECONDARY		0x85

#ifdef __cplusplus
}
#endif

#endif//__HUBHAL_FORMAT_H__
