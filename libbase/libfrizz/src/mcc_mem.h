/*!******************************************************************************
 * @file mcc_mem.h
 * @brief MCC Library for dynamic memory allocation
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MCC_MEM_H__
#define __MCC_MEM_H__

#ifdef __cplusplus
extern "C" {
#endif

/*!
	@brief	set memory pool
	@param [in]	addr	first address of memory pool
	@param [in]	size	memory pool size
	@return
		0:	normal
		<0:	abnormal
*/
int mcc_mem_init( void* addr, const unsigned int size );

/*!
	@brief	allocate memory
	@param [in]	align	alignment
	@param [in]	size	allocation size
	@return
		not NULL:	allocate first address
		NULL:		memory allocate failure
*/
void* mcc_mem_malloc( const unsigned int align, const unsigned int size );

/*!
	@brief	free memory
	@param [in]	addr	allocat first address
	@note	empty function
*/
void mcc_mem_free( void* addr );

#ifdef __cplusplus
}// extern "C"
#endif

#endif//__MCC_MEM_H__
