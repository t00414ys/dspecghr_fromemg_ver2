/*!******************************************************************************
 * @file fifo_mgr.h
 * @brief  software fifo manager
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

typedef struct {
	unsigned int*	top;
	unsigned int*	end;
	unsigned int*	w_p;
	unsigned int*	r_p;
	int				num;
} fifo_mgr_t;

/**
 * @brief Init FIFO Table
 *
 * @param [in] o object
 * @param [in] ram free area for fifo
 * @param [in] size size of word of free area
 */
void fifo_mgr_init( fifo_mgr_t *o, unsigned int* ram, unsigned int size );

/**
 * @brief query free space
 *
 * @param [in] o object
 *
 * @return size for free space
 */
static inline unsigned int fifo_mgr_get_free( const fifo_mgr_t* const o )
{
	unsigned int size;
	if( o->num == 0 ) {
		size = o->end - o->top;
	} else if( o->r_p < o->w_p ) {
		size = o->r_p - o->top;
		size += o->end - o->w_p;
	} else if( o->w_p < o->r_p ) {
		size = o->r_p - o->w_p;
	} else {
		size = 0;
	}
	return size;
}

/**
 * @brief Push Entry
 *
 * @param [in] o object
 * @param [in] sensor_id sensor's ID
 * @param [in] ts timestamp
 * @param [in] p Entry
 * @param [in] size size of entry
 *
 * @return current num
 *
 * @note overwrite on old datas if FIFO is FULL
 */
unsigned int fifo_mgr_push( fifo_mgr_t* o, unsigned char sensor_id, unsigned int ts, void* p, unsigned int size );

/**
 * @brief query current num of FIFO
 *
 * @param [in] o object
 *
 * @return current num
 */
static inline int fifo_mgr_get_num( const fifo_mgr_t* const o )
{
	return o->num;
}

/**
 * @brief Peek Entry
 *
 * @param [in] o object
 * @param [out] sensor_id sensor_id
 * @param [out] ts timestamp
 * @param [out] p entry
 * @param [out] size entry size
 *
 * @return result 0:OK, <0:error
 */
int fifo_mgr_peek( fifo_mgr_t* o, unsigned char *sensor_id, unsigned int *ts, void* p, unsigned int *size );

/**
 * @brief Drop Entry
 *
 * @param [in] o object
 *
 * @return result 0:OK, <0:error
 */
int fifo_mgr_drop( fifo_mgr_t* o );

#ifdef __cplusplus
}
#endif
