/*!******************************************************************************
 * @file    hubmgr_dev.c
 * @brief   hub_mgr device program for control magnet raw data
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include "frizz_type.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "hub_mgr_if.h"
#include "sensor_if.h"
#include "base_driver.h"

typedef struct {
	// ID
	unsigned char		id;
	// IF
	sensor_if_t			pif;
	// status
	int					f_active;
	int					f_need;
	int					tick;
	unsigned int		ts;
	unsigned char		data[40];
} device_sensor_t;

static device_sensor_t g_device;

static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	return 0;
}

static int get_data( void** data, unsigned int *ts )
{
	//
	if( data != 0 ) {
		*data = &g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return sizeof( g_device.data ) / sizeof( int );
}


static void set_parent_if( sensor_if_get_t *gettor )
{
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		g_device.f_active = f_active;
	}
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	// TODO: call to set device for update interval api
	g_device.tick = tick;
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static unsigned int notify_ts( unsigned int ts )
{
	g_device.ts = ts;
	g_device.f_need = 1;
	return ts + g_device.tick;
}


static int calculate( void )
{
	int				i, j;
	unsigned char	dev_info, err;

	//
	g_device.data[0] = 0xFF;
	g_device.data[1] = 0x80;
	g_device.data[2] = 0xFF;
	g_device.data[3] = 0x09;
	//
	g_device.data[4] = 0x0D;
	g_device.data[5] = 0x01;
	g_device.data[6] = 0x00;
	g_device.data[7] = 0x00;

	for( i = 0 ; i < 32 ; i++ ) {
		err = 0;
		for( j = 0 ; j < 8 ; j++ ) {
			if( j != 0 ) {
				err = err << 1;
			}
			dev_info = get_device_condition( i * 8 + j );
			if( ( dev_info & ( CONDITION_INITERR | CONDITION_PHYERR ) ) != 0 ) {
				err = err | 0x01;
			}
		}
		g_device.data[8 + i] = err;
	}
	g_device.f_need = 0;
	return 1;
}


sensor_if_t* hubmgr_dev_init( void )
{
	// ID
	g_device.id = HUB_MGR_ID;

	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.calculate = calculate;
	// param
	g_device.f_active = 0;
	g_device.tick = 1000 * 60;

	return &( g_device.pif );
}

