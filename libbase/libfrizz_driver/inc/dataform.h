/*!******************************************************************************
 * @file dataform.h
 * @brief dataformat for frizz
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __DATAFORM_H__
#define __DATAFORM_H__

#if! defined(RUN_ON_PC)
#include <xtensa/tie/frizz_simd4w_que.h>
#define FLOAT_TYPE	frizz_tie_fp
#else
#define FLOAT_TYPE	float
#endif

#ifdef __cplusplus
extern "C" {
#endif


/**
 * @brief Prefix Code
 */
#define FRIZZ_DATA_PREFIX		0xFF01

/**
 * @brief Header for Data Format
 */
typedef union {
	struct {
		unsigned int	w;
	};
	struct {
		unsigned char	num;		///< payload word num
		unsigned char	data_id;	///< data ID #FRIZZ_DATA_ID
		unsigned short	prefix;		///< 0xFF01
	};
} FRIZZ_DATA_HEADER;

/**
 * @brief res code for continue
 */
#define FRIZZ_DATA_ID_RES_CODE_CONTINUE		0x8000
/**
 * @brief res code for not support
 */
#define FRIZZ_DATA_ID_RES_CODE_NOTSUPPORT	0xF000

/**
 * @brief response data for command
 */
typedef union {
	struct {
		unsigned int	w[1];
	};
	struct {
		unsigned short	imm;		///< immediately value or res code
		unsigned char	param_num;	///< fetched num of parameters
		unsigned char	cmd_id;		///< command id
	};
} FRIZZ_DATA_FORM_RES;

/**
 * @brief uint x1
 */
typedef union {
	struct {
		unsigned int	w[1];
	};
	struct {
		unsigned int	val[1];
	};
} FRIZZ_DATA_FORM_UINT1;

/**
 * @brief float x1
 */
typedef union {
	struct {
		unsigned int	w[1];
	};
	struct {
		FLOAT_TYPE		val[1];
	};
} FRIZZ_DATA_FORM_FLOAT1;

/**
 * @brief float x3
 */
typedef union {
	struct {
		unsigned int	w[3];
	};
	struct {
		FLOAT_TYPE		val[3];
	};
} FRIZZ_DATA_FORM_FLOAT3;

/**
 * @brief float x6
 */
typedef union {
	struct {
		unsigned int	w[6];
	};
	struct {
		FLOAT_TYPE		val[6];
	};
} FRIZZ_DATA_FORM_FLOAT6;

/**
 * @brief float x9
 */
typedef union {
	struct {
		unsigned int	w[9];
	};
	struct {
		FLOAT_TYPE		val[9];
	};
} FRIZZ_DATA_FORM_FLOAT9;

/**
 * @brief float x10
 */
typedef union {
	struct {
		unsigned int	w[10];
	};
	struct {
		FLOAT_TYPE		val[10];
	};
} FRIZZ_DATA_FORM_FLOAT10;

/**
 * @brief PDR output
 */
typedef union {
	struct {
		unsigned int	w[7];
	};
	struct {
		FLOAT_TYPE		loc[2];				///< relative position 0:x, 1:y
		FLOAT_TYPE		vel[2];				///< relative speed 0:x, 1:y
		unsigned int	state;				///< PDR state
		unsigned int	step_cnt;			///< step count
		FLOAT_TYPE		total_dst;			///< accumulation distance
	};
} FRIZZ_DATA_FORM_PDR_OUTPUT;


/**
 * @brief DATA ID
 *
 * set to #FRIZZ_DATA_HEADER.data_id
 */
enum FRIZZ_DATA_ID {
	/**
	 * @brief response for command
	 *
	 * @param cmd_id response from command ID
	 * @param param_num current fetched number of paramters
	 * @param imm #FRIZZ_DATA_ID_RES_CONTINUE, #FRIZZ_DATA_ID_RES_NOTSPPORT
	 * useformat: #FRIZZ_DATA_FORM_RES
	 */
	FRIZZ_DATA_ID_RES = 0,

	/**
	 * @brief FW Version
	 *
	 * @param val[0] response from command ID
	 * useformat: #FRIZZ_DATA_FORM_UINT1
	 */
	FRIZZ_DATA_ID_FW_VERSION,

	/**
	 * @brief 9d sensors
	 *
	 * useformat: #FRIZZ_DATA_FORM_FLOAT10
	 * @param val[0*3+0] gyro data axis x
	 * @param val[0*3+1] gyro data axis y
	 * @param val[0*3+2] gyro data axis z
	 * @param val[1*3+0] accl data axis x
	 * @param val[1*3+1] accl data axis y
	 * @param val[1*3+2] accl data axis z
	 * @param val[2*3+0] magn data axis x
	 * @param val[2*3+1] magn data axis y
	 * @param val[2*3+2] magn data axis z
	 * @param val[9] sampling time
	 */
	FRIZZ_DATA_ID_9DSENSORS,

	/**
	 * @brief 3d accelerometer
	 *
	 * useformat: #FRIZZ_DATA_FORM_FLOAT3
	 * @param val[0] sensor data axis x
	 * @param val[1] sensor data axis y
	 * @param val[2] sensor data axis z
	 */
	FRIZZ_DATA_ID_ACCELEROMETER,

	/**
	 * @brief 3d magnetometer
	 *
	 * useformat: #FRIZZ_DATA_FORM_FLOAT3
	 * @param val[0] sensor data axis x
	 * @param val[1] sensor data axis y
	 * @param val[2] sensor data axis z
	 */
	FRIZZ_DATA_ID_MAGNETOMETER,

	/**
	 * @brief 3d gyroscope
	 *
	 * useformat: #FRIZZ_DATA_FORM_FLOAT3
	 * @param val[0] sensor data axis x
	 * @param val[1] sensor data axis y
	 * @param val[2] sensor data axis z
	 */
	FRIZZ_DATA_ID_GYROSCOPE,

	/**
	 * @brief barometer
	 *
	 * useformat: #FRIZZ_DATA_FORM_FLOAT1
	 * @param val[0] sensor data
	 */
	FRIZZ_DATA_ID_BAROMETER,

	/**
	 * @brief orientation
	 *
	 * useformat: #FRIZZ_DATA_FORM_FLOAT3
	 * @param val[0] yaw
	 * @param val[1] pitch
	 * @param val[2] rall
	 */
	FRIZZ_DATA_ID_ORIENTATION,

	/**
	 * @brief PDR data
	 *
	 * useformat: #FRIZZ_DATA_FORM_PDR_OUTPUT
	 */
	FRIZZ_DATA_ID_PDR_OUTPUT,

	/**
	 * @brief Hard-Iron Offset Parameters
	 *
	 * useformat: #FRIZZ_DATA_FORM_FLOAT3
	 * @param val[0] offset data axis x
	 * @param val[1] offset data axis y
	 * @param val[2] offset data axis z
	 */
	FRIZZ_DATA_ID_HARDIRON_PARAM,

	/**
	 * @brief Soft-Iron Offset Parameters
	 *
	 * useformat: #FRIZZ_DATA_FORM_FLOAT9
	 * @param val Soft-Iron 9 parameters
	 */
	FRIZZ_DATA_ID_SOFTIRON_PARAM,

	/**
	 * @brief NOTIFY for PDR ready to Start
	 *
	 * useformat: none
	 */
	FRIZZ_DATA_ID_DEMO_PDR_READY_TO_START,

	/**
	 * @brief Power Consumption
	 *
	 * useformat: #FRIZZ_DATA_FORM_FLOAT6
	 * @param val[0] current @ 5.0 V for Board
	 * @param val[1] current @ 3.3 V for Peripheral
	 * @param val[2] current @ 1.2 V for Core
	 */
	FRIZZ_DATA_ID_DEMO_POWER_CONSUMPTION,

};


#ifdef __cplusplus
}
#endif

#endif//__DATAFORM_H__
