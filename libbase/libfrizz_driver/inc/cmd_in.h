/*!******************************************************************************
 * @file cmd_in.h
 * @brief input for command to FrizzFW
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __CMD_IN_H__
#define __CMD_IN_H__

#include "command.h"

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief input route
 */
typedef enum {
	CMD_IN_INPUT_MES = 0,	///< use message register
	CMD_IN_INPUT_QUEUE		///< use input queue for DEBUG
} CMD_IN_INPUT_TYPE;

/**
 * @brief res code for not support
 */
#define CMD_IN_RES_CODE_NOTSUPPORT	0xF000

/**
 * @brief initialize
 *
 * @param input type: #CMD_IN_INPUT_TYPE
 */
void cmd_in_init( CMD_IN_INPUT_TYPE type );

/**
 * @brief get frizz command
 *
 * @param id command ID
 * @param imm immediate
 * @param data output data
 *
 * @return 0: command exist, 1:nothing command
 */
int cmd_in_get_cmd( FRIZZ_COMMAND_ID *id, unsigned short *imm, FRIZZ_COMMAND_FORM **data );

/**
 * @brief output response for command
 *
 * @param imm result 0x8000 -> changed -1 value (don't use)
 *
 * @note if getting command in cmd_in_get_cmd(),this function must call.
 *  the command is not accepted until I carry it out.
 */
void cmd_in_res_cmd( unsigned short imm );

#ifdef __cplusplus
}
#endif

#endif//__CMD_IN_H__
