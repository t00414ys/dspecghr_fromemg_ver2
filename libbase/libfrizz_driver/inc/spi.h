/*!******************************************************************************
 * @file spi.h
 * @brief header for SPI master driver
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __SPI_H__
#define __SPI_H__
/** @addtogroup SPI
 */
/** @ingroup SPI */
/* @{ */

#ifdef __cplusplus
extern "C" {
#endif

/**
 * @brief	Initialize SPI and change the PIN function of GPIO correspond to specified CS.<BR>
 *			(Each GPIO0~3 PIN is alternative PIN of GPIO or SPI CS).
 *
 * @param [in]	core_freq	frizz core clock frequency<BR>
 *							When use external OSC as frizz core clock, set the frequency of the external OSC.<BR>
 *							When use internal OSC as frizz core clock, set 40MHz.<BR>
 * @param [in]	target_freq	SSCLK frequency of SPI to be set<BR>
 *							The maximum frequency is one-fifth of frizz core clock frequency.
 * @param [in]	mode 		Set SPI mode specifies latch timing of received data
 *							0:SPI mode0
 *							2:SPI mode2
 * @param [in]	in_cs_no	Number of chip select(0 - 3).
 * @return void
 */
void spi_init( unsigned int core_freq, unsigned int target_freq, unsigned char mode, unsigned int in_cs_no );

/**
 * @brief	Transmit by SPI.
 *			This function is blocked until all data transmission is completed.
 *
 * @param [in]	tx_data		Pointer to send buffer
 * @param [out]	rx_data		Pointer to receive buffer
 * @param [in]	size		Size of transmission
 * @param [in]	cs_num		Number of CS to transmit
 * @param [in]	f_cs_keep	CS signal holding flag between each byte transmission<BR>
 *							0 : Not hold CS signal between each byte transmission<BR>
 *							1 : Hold CS signal between each byte transmission
 * @return void
 */
void spi_trans_data( unsigned char* tx_data, unsigned char* rx_data, unsigned int size, unsigned int cs_num, int f_cs_keep );

#ifdef __cplusplus
}
#endif //__cplusplus

/* @} */
#endif // #ifndef __SPI_H__
