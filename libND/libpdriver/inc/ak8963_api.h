/*!******************************************************************************
 * @file    ak8963_api.h
 * @brief   ak8963 sensor api header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __AK8963_API_H__
#define __AK8963_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

int ak8963_init( unsigned int param );
void ak8963_ctrl( int f_ena );
unsigned int ak8963_rcv( unsigned int tick );
int ak8963_conv( frizz_fp data[3] );

int ak8963_setparam( void *ptr );
unsigned int ak8963_get_ver( void );
unsigned int ak8963_get_name( void );

int ak8963_calib_get_status( void *result );
int ak8963_calib_get_data( void *data );
int ak8963_calib_set_data( void *data );

int ak8963_get_condition( void *data );

#ifdef __cplusplus
}
#endif

#endif
