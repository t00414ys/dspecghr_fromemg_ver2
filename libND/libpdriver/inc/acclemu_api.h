/*!******************************************************************************
 * @file    acclemu_api.h
 * @brief   sample program for accel/gyro sensor emulation
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ACCLEMU_API_H__
#define __ACCLEMU_API_H__

#include "frizz_type.h"

#ifdef __cplusplus
extern "C" {
#endif

//
int  acclemu_init( unsigned int param );
void acclemu_ctrl_accl( int f_ena );
unsigned int acclemu_rcv_accl( unsigned int tick );
int  acclemu_conv_accl( frizz_fp data[3] );

int acclemu_setparam_accl( void *ptr );
unsigned int acclemu_get_ver( void );
unsigned int acclemu_get_name( void );

int acclemu_get_condition( void *data );
int acclemu_accl_get_raw_data( void *data );


#ifdef __cplusplus
}
#endif

//
#endif
