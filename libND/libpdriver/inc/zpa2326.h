/*!******************************************************************************
 * @file    zpa2326.h
 * @brief   zpa2326 sensor driver header
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __ZPA2326_H__
#define __ZPA2326_H__
#include "zpa2326_api.h"


/*
 *
 *		The rest of this is okay not change
 *
 */

#define PRES_ZPA2326_I2C_ADDRESS	0x5C		///< Pressure Sensor(made from MURATA)

#define ZPA2326_DEVICE_ID			0xB9		// Device ID

#define ZPA2326_REG_DEVICE_ID		0x0F		// DEVICE ID Register
#define ZPA2326_REG_CTRL_REG0		0x20 		//
#define ZPA2326_REG_CTRL_REG1		0x21 		//
#define ZPA2326_REG_CTRL_REG2		0x22 		//
#define ZPA2326_REG_CTRL_REG3		0x23 		//

#define ZPA2326_REG_PRESS_OUT_XL	0x28		// Pressure value XL
#define ZPA2326_REG_PRESS_OUT_L		0x29		// Pressure value L
#define ZPA2326_REG_PRESS_OUT_H		0x2A		// Pressure value H
#define ZPA2326_REG_TEMP_OUT_L		0x2B		// Temperature value L
#define ZPA2326_REG_TEMP_OUT_H		0x2C		// Temperature value H

#endif // __ZPA2326_H__

