/*!******************************************************************************
 * @file    magn_driver.h
 * @brief   sample program for magnet sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __MAGN_DRIVER_H__
#define __MAGN_DRIVER_H__

#include "config.h"
#include "base_driver.h"

#if defined(USE_HMC5883)
#include "hmc5883_api.h"
#if	!defined(MAP_HMC5883)
#define	MAP_HMC5883		(0)
#endif
#define	HMC5883_DATA	{	hmc5883_init,					hmc5883_ctrl_magn,		hmc5883_rcv_magn ,		hmc5883_conv_magn, 	\
							hmc5883_setparam_magn,			hmc5883_get_ver,		hmc5883_get_name ,		MAP_HMC5883,		\
							{hmc5883_magn_get_condition,	0,						0,						0}					},
#else
#define	HMC5883_DATA
#endif


#if defined(USE_AK09911) || defined(USE_AK09912)
#include "ak0991X_api.h"
#if	!defined(MAP_AK0991X)
#define	MAP_AK0991X		(0)
#endif
#define	AK0991X_DATA	{	ak0991X_init,				ak0991X_ctrl,				ak0991X_rcv ,				ak0991X_conv,	 		\
							ak0991X_setparam,			ak0991X_get_ver,			ak0991X_get_name ,			MAP_AK0991X,			\
							{ak0991x_get_condition,		ak0991x_calib_get_status,	ak0991x_calib_get_data,		ak0991x_calib_set_data}	},
#else
#define	AK0991X_DATA
#endif

#if defined(USE_AK8963)
#include "ak8963_api.h"
#if	!defined(MAP_AK8963)
#define	MAP_AK8963		(0)
#endif
#define	AK8963_DATA		{	ak8963_init,				ak8963_ctrl,				ak8963_rcv ,				ak8963_conv,	 		\
							ak8963_setparam,			ak8963_get_ver,				ak8963_get_name ,			MAP_AK8963,				\
							{ak8963_get_condition,		ak8963_calib_get_status,	ak8963_calib_get_data,		ak8963_calib_set_data}	},
#else
#define	AK8963_DATA
#endif

#if defined(USE_MMC351X)
#include "mmc3516x_api.h"
#if	!defined(MAP_MMC351X)
#define	MAP_MMC351X		(0)
#endif
#define	MMC351X_DATA	{	mmc3516x_init,				mmc3516x_ctrl,				mmc3516x_rcv ,				mmc3516x_conv, 			\
							mmc3516x_setparam,			mmc3516x_get_ver,			mmc3516x_get_name ,			MAP_MMC351X,			\
							{mmc3516x_get_condition,	mmc3516x_calib_get_status,	mmc3516x_calib_get_data,	mmc3516x_calib_set_data}},
#else
#define	MMC351X_DATA
#endif

#if defined(USE_BMM150)
#include "bmm150_api.h"
#if	!defined(MAP_BMM150)
#define	MAP_BMM150		(0)
#endif
#define	BMM150_DATA		{	bmm150_init,				bmm150_ctrl,				bmm150_rcv ,				bmm150_conv, 			\
							bmm150_setparam,			bmm150_get_ver,				bmm150_get_name ,			MAP_BMM150,				\
							{bmm150_get_condition,		bmm150_calib_get_status,	bmm150_calib_get_data,		bmm150_calib_set_data}	},
#else
#define	BMM150_DATA
#endif

#if defined(USE_PM131)
#include "pm131_api.h"
#if	!defined(MAP_PM131)
#define	MAP_PM131		(0)
#endif
#define	PM131_DATA		{	pm131_init,				pm131_ctrl,				pm131_rcv ,				pm131_conv, 			\
							pm131_setparam,			pm131_get_ver,				pm131_get_name ,			MAP_PM131,				\
							{pm131_get_condition,		pm131_calib_get_status,	pm131_calib_get_data,		pm131_calib_set_data}	},
#else
#define	PM131_DATA
#endif

#if defined(USE_MAGN_EMU)
#include "magnemu_api.h"
#if	!defined(MAP_MAGNEMU)
#define	MAP_MAGNEMU		(0)
#endif
#define	MAGNEMU_DATA	{	magnemu_init,				magnemu_ctrl,				magnemu_rcv ,				magnemu_conv, 			\
							magnemu_setparam,			magnemu_get_ver,			magnemu_get_name ,			MAP_MAGNEMU,			\
							{magnemu_get_condition,		magnemu_calib_get_status,	magnemu_calib_get_data,		magnemu_calib_set_data}	},
#else
#define	MAGNEMU_DATA
#endif



#endif // __MAGN_DRIVER_H__
