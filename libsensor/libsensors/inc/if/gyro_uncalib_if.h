/*!******************************************************************************
 * @file    gyro_uncalib_if.h
 * @brief   virtual gyro uncalibration sensor interface
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#ifndef __GYRO_UNCALIB_IF_H__
#define __GYRO_UNCALIB_IF_H__

#include "libsensors_id.h"

#ifdef RUN_ON_FRIZZ
#define FLOAT	frizz_tie_fp	//!< FLOAT type
#else
#define FLOAT	float			//!< FLOAT type
#endif

/** @defgroup GYRO_UNCALIB GYRO UNCALIB
 *  @{
 */
#define GYRO_UNCALIB_ID	SENSOR_ID_GYRO_UNCALIB	//!< A gyro uncalibration sensor interface ID


/**
 * @struct gyro_uncalibrated_data_t
 * @brief Output data structure for gyro uncalibration sensor
 */
typedef struct {
	FLOAT		data[6];	//!< uncalibrated : Angular velocity vector [rad/s] data[0]:X-axis Component[rad/s], data[1]:Y-axis Component[rad/s], data[2]:Z-axis Component[rad/s]
	//!< offset : Angular velocity vector [rad/s] data[3]:X-axis Component[rad/s], data[4]:Y-axis Component[rad/s], data[5]:Z-axis Component[rad/s]
} gyro_uncalibrated_data_t;
/**
 * @name Command List
 * @note none
 */
//@{
//@}

/** @} */
#endif
