/*!******************************************************************************
 * @file    accl_stat.h
 * @brief   virtual accel statistics sensor
 * @par     Copyright
 *          (C) 2015 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup ACCEL_STATISTICS ACCEL STATISTICS
 *  @ingroup virtualgroup Virtual Sensor
 *  @brief accel statistic sensor<br>
 *  @brief -# <B>Contents</B><br>
 *  @brief Debug use only<br>
 *  <br>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><br>
 *  @brief Sensor ID : #SENSOR_ID_ACCEL_STATISTICS<br>
 *  @brief Parent Sensor ID : #SENSOR_ID_ACCEL_RAW<br>
 *  <br>
 *  \dot
 *  digraph structs {
 *  	node [shape=record,  fontsize=10];
 *		struct1 [label="SENSOR_ID_ACCEL_STATISTICS" color=red];
 *		struct2 [label="SENSOR_ID_ACCEL_RAW" color=blue];
 *		struct2 -> struct1;
 *	}
 *  \enddot
 *  <br>
 *  @brief -# <B>Detection Timing</B><br>
 *  @brief Detection Timing : Continuous (This sensor type output sensor
 * data every priod when host determines sensor data or frizz optimized sensor data.)
 */
#ifndef __ACCL_STAT_H__
#define __ACCL_STAT_H__

#include "sensor_if.h"

/** @defgroup ACCEL_STATISTICS ACCEL STATISTICS
 *  @{
 */

/**
 *@brief	Initialize accel stat sensor
 *@par		External public functions
 *
 *@retval	sensor_if_t		Sensor Interface
 *
 */
sensor_if_t* accl_stat_init( void );

/** @} */
#endif
