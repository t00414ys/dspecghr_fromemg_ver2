/*!******************************************************************************
 * @file    magn_calib_raw.h
 * @brief   virtual magnet calibration raw sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/

/** @defgroup MAGNET_CALIB_RAW MAGNET CALIB RAW
 *  @ingroup virtualgroup Virtual Sensor
 *  @brief magnet calibration raw sensor<br>
 *  @brief -# <B>Contents</B><br>
 *  @brief A sensor for taking out magnet calibration status.<br>
 *  <br>
 *  @brief -# <B>Sensor ID and Parent-child relationship</B><br>
 *  @brief Sensor ID : #SENSOR_ID_MAGNET_CALIB_RAW<br>
 *  @brief Parent Sensor ID : #SENSOR_ID_MAGNET_RAW<br>
 *  <br>
 *  \dot
 *  digraph structs {
 *  	node [shape=record,  fontsize=10];
 *		struct1 [label="SENSOR_ID_MAGNET_CALIB_RAW" color=red];
 *		struct2 [label="SENSOR_ID_MAGNET_RAW" color=blue];
 *		struct2 -> struct1;
 *	}
 *  \enddot
 *  <br>
 *  @brief -# <B>Detection Timing</B><br>
 *  @brief Continuous (This sensor type output sensor
 * data every priod when host determines sensor data or frizz optimized sensor data.)<br>
 */

#ifndef __MAGN_CALIB_RAW_H__
#define __MAGN_CALIB_RAW_H__

#include "frizz_type.h"
#include "sensor_if.h"
#include "libsensors_id.h"

/** @defgroup MAGNET_CALIB_RAW MAGNET CALIB RAW
 *  @{
 */

/**
 *@brief	Initialize Raw-Iron magnet calibration sensor
 *@par		External public functions
 *
 *@retval	sensor_if_t		Sensor Interface
 *
 */
sensor_if_t* magn_calib_raw_init( void );
/** @} */
#endif
