/*!******************************************************************************
 * @file    orientation.c
 * @brief   virtual orientation sensor
 * @par     Copyright
 *          (C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <stdio.h>
#include "frizz_type.h"
#include "frizz_const.h"
#include "frizz_math.h"
#include "sensor_if.h"
#include "sensor_util.h"
#include "hub_mgr.h"
#include "hub_util.h"
#include "libsensors_id.h"
#include "if/gyro_posture_if.h"
#include "gpio.h"
#include "quart.h"
#include "uart.h"
//#include "matrix.h"
#include "../../../../libbase/libhub/inc/hub_mgr_if.h"
#include "../../../../libbase/libfrizz_driver/inc/frizz_peri.h"
//#include "../../../../libbase/libfrizz_driver/inc/timer.h"

//#define		SENSOR_VER_MAJOR		(1)						// Major Version
//#define		SENSOR_VER_MINOR		(0)						// Minor Version
//#define		SENSOR_VER_DETAIL		(0)						// Detail Version

#define DEF_INIT(x) x ## _init

typedef struct {
	// ID
	unsigned char		id;
	unsigned char		par_ls[1];
	// IF
	sensor_if_t			pif;
	sensor_if_get_t		*p_par;

	int					f_active;
	int					tick;
	int					f_need;
	unsigned int		ts;
	// data
	frizz_fp			data[11];//Host に送るデータ数（4bite）
} device_sensor_t;

//#define num 2 //フィルタに使うデータの数(11, 25, 51)
//#define nn 1 // num - 1 (10, 24, 50)

//uart 処理のための定数定義
#define SYNC	170 //0xAA
//#define EXCODE 85  //0x55
//uart 関連の変数定義
unsigned char uart_data_temp[4];
unsigned char uart_data[18];
unsigned char uart_data2[18];
unsigned int uart_count_pre;
unsigned int uart_count_post;
UART_LS uart_status_pre;
UART_LS uart_status_post;
int counter;

static device_sensor_t	g_device;
static frizz_fp			t;
int status = 0;


static unsigned char get_id( void )
{
	return g_device.id;
}

static unsigned int get_parent_list( unsigned char **list )
{
	*list = g_device.par_ls;
	return sizeof( g_device.par_ls );
}

static int get_data( void** data, unsigned int *ts )
{
	if( data != 0 ) {
		*data = g_device.data;
	}
	if( ts != 0 ) {
		*ts = g_device.ts;
	}
	return 11; //host に送るデータ数（dataの変数宣言部分と数字を合わせる）
}

static int need_calc( void )
{
	return g_device.f_need;
}

static void set_parent_if( sensor_if_get_t *gettor )
{
	if( gettor->id() == g_device.par_ls[0] ) {
		g_device.p_par = gettor;
	}
}

static void set_active( int f_active )
{
	if( g_device.f_active != f_active ) {
		g_device.f_active = f_active;
		hub_mgr_set_sensor_active( g_device.id, g_device.p_par->id(), g_device.f_active );
	}
	// activate の際に変数の初期化
	//first = 0;
	flag_cal = 0;
	status = 0;
}

static int get_active( void )
{
	return g_device.f_active;
}

static int set_interval( int tick )
{
	g_device.tick = 1;
	return g_device.tick;
}

static int get_interval( void )
{
	return g_device.tick;
}

static int command( unsigned int cmd, void* param )
{
	int ret = -1;
	switch( SENSOR_MGR_CMD_CODE_TO_CMD( cmd ) ) {
	case SENSOR_GET_VERSION:
		//ret =	make_version( SENSOR_VER_MAJOR, SENSOR_VER_MINOR, SENSOR_VER_DETAIL );
		ret =	make_version( 0x00, 0x00, 0x00 );
		break;
	default:
		ret = RESULT_ERR_CMD;
	}
	return ret;
}

static unsigned int notify_ts( unsigned int ts )
{
	g_device.f_need = 1;
	return ts + g_device.tick;
}

static void interrupt_event( void *ptr )
{
	// to change high clock mode has already changed in the int-lowpri-dispatcher.S file with an assembler (library libs/libxtos_int1.a)
	//	*REGXMODE0 = (*REGXMODE0 & 0x0A00) & (~0x0800);	// internal high clock OSC power on
	//	*REGXMODE0 = (*REGXMODE0 & 0x0A00) | 0x0200;	// to high clock mod
	gpio_set_interrupt( DISABLING_INTERRUPT );
	//set_active( 1 );
	//gpio_set_interrupt( ENABLING_INTERRUPT );
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
static frizz_fp4w_t *accl_data;		//ads8332で取得したセンサデータ（加速度，ジャイロ，地磁気）が入ります。
__attribute__( ( aligned( 16 ) ) ) static frizz_fp		a[4];
//__attribute__( ( aligned( 16 ) ) ) static frizz_fp		g[4];	//[rad/s]
__attribute__( ( aligned( 16 ) ) ) static frizz_fp		g_raw[4];
__attribute__( ( aligned( 16 ) ) ) static frizz_fp		a_raw[4];
__attribute__( ( aligned( 16 ) ) ) static frizz_fp		g_off[4];
__attribute__( ( aligned( 16 ) ) ) static frizz_fp		a_off[4];
//__attribute__( ( aligned( 16 ) ) ) static frizz_fp		e_raw[4];

__attribute__( ( aligned( 16 ) ) ) static frizz_fp4w			*a4w     = (frizz_fp4w *) &a[0];
//__attribute__( ( aligned( 16 ) ) ) static frizz_fp4w			*g4w     = (frizz_fp4w *) &g[0];
__attribute__( ( aligned( 16 ) ) ) static frizz_fp4w			*g_raw4w = (frizz_fp4w *) &g_raw[0];
__attribute__( ( aligned( 16 ) ) ) static frizz_fp4w			*a_raw4w = (frizz_fp4w *) &a_raw[0];
__attribute__( ( aligned( 16 ) ) ) static frizz_fp4w			*g_off4w = (frizz_fp4w *) &g_off[0];
__attribute__( ( aligned( 16 ) ) ) static frizz_fp4w			*a_off4w = (frizz_fp4w *) &a_off[0];

static frizz_fp			e_raw;

//sensor_util_half_t HR;
static int ECG_raw;
static unsigned char HR_BMD;
static int flag_uart = 0;


union{
	frizz_fp frizzfp_;
	unsigned char data_ver[4];
	struct{
		unsigned char ll;
		unsigned char lh;
		unsigned char hl;
		unsigned char hh;
	}s;
}hoge;

//frizz_fp	temp;
frizz_fp	temp_cal;
//static int fifo_flag1;
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

static int calculate( void )
{

	//gpio_set_data( GPIO_NO_3, 1 );
#if 1
	//gpio_set_data( GPIO_NO_3, 0 );
	//int				    size, i, k;
	//static int         j = 0;

	g_device.p_par->data( ( void** )&accl_data, &g_device.ts );

	//センサからのローデータを格納
	a_raw[0]  = ((frizz_fp*)accl_data)[0];
	a_raw[1]  = ((frizz_fp*)accl_data)[1];
	a_raw[2]  = ((frizz_fp*)accl_data)[2];
	g_raw[0]  = ((frizz_fp*)accl_data)[3];
	g_raw[1]  = ((frizz_fp*)accl_data)[4];
	g_raw[2]  = ((frizz_fp*)accl_data)[5];
	e_raw  = ((frizz_fp*)accl_data)[6];
	//e_raw[1]  = ((frizz_fp*)accl_data)[9];
	//e_raw[2]  = ((frizz_fp*)accl_data)[10];

	if(flag_offset == 1){//calibration ルーチンのif文
		//*g4w = *g_raw4w;
		*a4w = *a_raw4w;

		if(flag_cal == 0){
			*g_off4w = FRIZZ_CONST_ZERO;
			*a_off4w = FRIZZ_CONST_ZERO;
		}

		a[2] = a[2] + FRIZZ_CONST_ONE;
		temp_cal = frizz_div(FRIZZ_CONST_ONE, (as_frizz_fp(flag_cal) + FRIZZ_CONST_ONE));
		//*g_off4w = ((*g_off4w * as_frizz_fp(flag_cal)) + *g4w) * temp_cal;
		*a_off4w = ((*a_off4w * as_frizz_fp(flag_cal)) + *a4w) * temp_cal;
		*g_off4w = ((*g_off4w * as_frizz_fp(flag_cal)) + *g_raw4w) * temp_cal;
		//*a_off4w = ((*a_off4w * as_frizz_fp(flag_cal)) + *a_raw4w) * temp_cal;

	}else{//通常のルーチン
		//offsetの適応
		*g_raw4w = *g_raw4w - *g_off4w;
		*a_raw4w = *a_raw4w - *a_off4w;

		//*g4w = *g_raw4w;
		//*acl4w = *a_raw4w;

		//gpio_set_data( GPIO_NO_3, 0 );

	} //calibration if の終わり

	//UART(BMD101)の処理
	counter = 0;
	flag_uart = -1;
	//UART のステータスを確認
	uart_status_pre = uart_get_status(&uart_count_pre);
	//UARTに7データあるものだけを解析
	if (uart_count_pre > 8){
		while(counter < 8){//8データ解析してデータがない場合は解析しない(ひとつ前のデータを書き込む)
			counter = counter + 1;
			uart_get_data( &uart_data_temp[0], 1);
			if (uart_data_temp[0] == SYNC){	//[AA] ([170])
				uart_get_data( &uart_data_temp[1], 1);
				if (uart_data_temp[1] == SYNC){	//[AA][AA] ([170][170])
					uart_get_data( &uart_data_temp[2], 2);
					// Raw Packet
					if (uart_data_temp[2] == 4 && uart_data_temp[3] == 128){	//[AA][AA][04][80] ([170][170][04][128])
						flag_uart = 1;
						uart_get_data( &uart_data[0], 4);
						uart_status_post = uart_get_status(&uart_count_post);
						//HR.ubyte[0] = uart_data[1];
						//HR.ubyte[1] = uart_data[2];
						ECG_raw =  uart_data[1] * 256 +  uart_data[2];
						if (ECG_raw >= 32768){
							ECG_raw = ECG_raw - 65536;
						}
						break;
						// Analyzed Packet
					}else if ((uart_data_temp[2] == 18 && uart_data_temp[3] == 2)){	//[AA][AA][12][02] ([170][170][18][02])
						flag_uart = 10;
#if 0
						uart_get_data( &uart_data2[0], 18);
#elif 0
						uart_get_data( &uart_data2[0], 1);
						uart_get_data( &uart_data2[1], 17);

#elif 0
						uart_get_data( &uart_data2[0], 1);
						uart_get_data( &uart_data2[1], 1);
						uart_get_data( &uart_data2[2], 16);
#elif 1
						uart_get_data( &uart_data2[0], 1);
						uart_get_data( &uart_data2[1], 1);
						uart_get_data( &uart_data2[2], 1);
						uart_get_data( &uart_data2[3], 15);
#endif
						uart_status_post = uart_get_status(&uart_count_post);
						//計測エラーなしなら心拍を更新
						if (uart_data2[0] == 200 && uart_data2[1] == 3){	//[AA][AA][12][02][C8][03] ([170][170][18][02][200][03])
							HR_BMD = uart_data2[2];//心拍データの更新
						}
						counter = 100;
						break;
					}
				}

			}

		}

	}


#endif

#if 0 //FIFO をチェック
	//	if ( *((volatile unsigned int*)0x003F) & 0x00008000){
	//		// overrun
	//	}
	//	if ( *((volatile unsigned int*)FIFO_CNR) & 0x00008000){
	//			// overrun
	//	}

	//static int fifo_flag1;
	fifo_flag1 = *(volatile unsigned int*)REGFIFO_CNT; // 使い方あってる。


#endif

#if 0
	if (normA > (FRIZZ_CONST_TWO + FRIZZ_CONST_ONE)){
		hoge.s.hh = 0x01;
	}else{
		hoge.s.hh = 0x00;
	};
#else
	hoge.s.hh = 0x00;
#endif
	hoge.s.lh = 0x02;
	hoge.s.hl = 0x03;
	hoge.s.ll = flag_cal;

	g_device.data[ 0] = hoge.frizzfp_;
	// 出力された値を格納（センサ計測値はローデータ）
	// 角速度（deg/s）（ローデータ）
	//出力するデータの代入
#if 1//通常出荷版（ECGAG）
	g_device.data[ 1] = as_frizz_fp(ECG_raw);
	g_device.data[ 2] = HR_BMD;
	g_device.data[ 3] = e_raw;//as_frizz_fp(flag_cal);//e_raw[0];
	g_device.data[ 4] = flag_uart;
	// 加速度（ローデータ）
	g_device.data[ 5] = a_raw[0] ;	//((frizz_fp*)accl_data)[0];//acl[0];
	g_device.data[ 6] = a_raw[1] ;	//((frizz_fp*)accl_data)[1];//acl[1];
	g_device.data[ 7] = a_raw[2] ;	//((frizz_fp*)accl_data)[2];//acl[2];
	// 角速度（ローデータ）
	g_device.data[ 8] = g_raw[0];	//((frizz_fp*)accl_data)[8];
	g_device.data[ 9] = g_raw[1];	//((frizz_fp*)accl_data)[9];
	g_device.data[10] = g_raw[2];	//((frizz_fp*)accl_data)[10];

#elif 0
	g_device.data[ 1] = g_d[0];
	g_device.data[ 2] = g_d[1];
	g_device.data[ 3] = g_d[2];
	// 加速度（ローデータ）
	g_device.data[ 4] = a_raw[0] ;	//((frizz_fp*)accl_data)[0];//acl[0];
	g_device.data[ 5] = a_raw[1] ;	//((frizz_fp*)accl_data)[1];//acl[1];
	g_device.data[ 6] = a_raw[2] ;	//((frizz_fp*)accl_data)[2];//acl[2];
	// 地磁気（ローデータ）
	g_device.data[ 7] = flag_cal;//m_raw[0];	//flag;	//((frizz_fp*)accl_data)[8];
	g_device.data[ 8] = temptemp;//m_raw[1];	//((frizz_fp*)accl_data)[9];
	g_device.data[ 9] = fifo_flag1;	//((frizz_fp*)accl_data)[10];
	// クォータニオン（フィルタデータ）
	g_device.data[10] = q_fil[0];
	g_device.data[11] = q_fil[1];
	g_device.data[12] = q_fil[2];
	g_device.data[13] = q_fil[3];
#endif

	g_device.f_need = 0;

#if 1

	flag_cal++;
	first++;
	gpio_set_data( GPIO_NO_3, 0 );
	return 1;
#else
	if (freq == 1){
		flag_cal++;
		status += freq;
		gpio_set_data( GPIO_NO_3, 0 );
		return 1;
	}else{
		if (status <= flag_cal){
			//gpio_set_data( GPIO_NO_3, 1 );
			flag_cal++;  //caliculation の繰り返し回数0始まり
			status += freq;
			gpio_set_data( GPIO_NO_3, 0 );
			return 1;
		}else{
			flag_cal++;  //caliculation の繰り返し回数0始まり
			return 0;
			//return 1;
		}
	}
#endif


}

sensor_if_t* DEF_INIT( gyro_posture )(void) {
	//gpio_set_mode( GPIO_NO_3, GPIO_MODE_IN );

	//gpio_init( interrupt_event, 0 );
	gpio_set_mode( GPIO_NO_3, GPIO_MODE_OUT );	// Interrupt Setting
	//_xtos_set_intlevel( INTERRUPT_LEVEL_GPIO3_EDGE );
	//gpio_set_interrupt( ENABLING_INTERRUPT );

	// ID
	g_device.id = GYRO_POSTURE_ID;
	g_device.par_ls[0] = SENSOR_ID_ACCEL_RAW;	//0x80

	//UART
	quart_init( g_ROSC2_FREQ, QUART_BAUDRATE_57600, 0 );

	// IF
	g_device.pif.get.id = get_id;
	g_device.pif.get.parent_list = get_parent_list;
	g_device.pif.get.active = get_active;
	g_device.pif.get.interval = get_interval;
	g_device.pif.get.data = get_data;
	g_device.pif.get.need_calc = need_calc;
	g_device.pif.set.parent_if = set_parent_if;
	g_device.pif.set.active = set_active;
	g_device.pif.set.interval = set_interval;
	g_device.pif.notify_ts = notify_ts;
	g_device.pif.notify_updated = 0;
	g_device.pif.calculate = calculate;
	g_device.pif.command = command;
	g_device.pif.end = 0;
	// param
	g_device.f_active = 0;
	// sampling freq & time interval を設定
	g_device.tick = 1;  //1000 Hz
	t = frizz_div(as_frizz_fp(g_device.tick), FRIZZ_CONST_THOUSAND );
	g_device.f_need = 0;
	g_device.ts = 0;

	return &(g_device.pif);
}


#if 0//デバック用の変数を格納
union{
	frizz_fp frizzfp_;
	unsigned char data_ver[4];
	struct{
		unsigned char ll;
		unsigned char lh;
		unsigned char hl;
		unsigned char hh;
	}ss;
}hogehoge;
hogehoge.ss.hh = 0x00;
hogehoge.ss.hl = 0x00;
hogehoge.ss.lh = 0x00;
hogehoge.ss.ll = 0x00;

static unsigned short aaa = 0;
hogehoge.ss.hh = 0x55;
hogehoge.ss.hl = (unsigned char)(aaa>>8);
hogehoge.ss.lh = (unsigned char)(aaa++>>0);
hogehoge.ss.ll = 0x55;

hogehoge.ss.hh = 0xAA;
hogehoge.ss.hl = 0xAA;
hogehoge.ss.lh = 0xAA;
hogehoge.ss.ll = 0xAA;
g_device.data[13] = hogehoge.frizzfp_;//q_fil[3];

//時間を計測
cycle = frizz_time_measure( cycle );
sprintf( time, "%d.%06d[msec]\n", cycle/1000000, cycle%1000000 );
quart_out_raw( "%s", time );

#endif

