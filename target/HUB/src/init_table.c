/*!******************************************************************************
 * @file	init_table.c
 * @brief	main routine
 * @par 	Copyright
 *			(C) 2014 MegaChips Corporation - All rights reserved.
 *
 * This software is authored by MegaChips Corporation intellectual property,
 * including the copyrights in all countries in the world.
 * This software is provided under a license to use only with all other rights,
 * including ownership rights, being retained by MegaChips Corporation.
 *
 * This file may not be distributed, copied, or reproduced in any manner,
 * electronic or otherwise, without the written consent of MegaChips Corporation.
 *******************************************************************************/
#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include "i2c.h"
#include "gpio.h"
#include "timer.h"
#include "frizz_peri.h"
#include "frizz_env.h"

#include "hub_mgr.h"
#include "hub_util.h"
#include "frizz_util.h"
#include "hubhal_out.h"
#include "hubhal_out_txt.h"
#include "frizz_debug.h"

// sensor group header
#include "group_accl.h"
#include "group_magn.h"
#include "group_gyro.h"
#include "group_rotation.h"
#include "cyclic_sensor.h"

#if defined (USE_DEBUG_IN)
#include "group_dummy.h"
#define USE_QUART_IN	// input via UART as QueueIF
#if defined (USE_QUART_IN)
#include "quart.h"
#endif
#endif

#ifdef __cplusplus
extern "C" {
#endif

extern	sensor_if_t* accl_raw_init( void );
extern	sensor_if_t* magn_raw_init( void );
extern	sensor_if_t* gyro_raw_init( void );
extern	sensor_if_t* pres_raw_init( void );
extern	sensor_if_t* ppg_raw_init( void );

#ifdef __cplusplus
}
#endif


sensor_if_t* ( *init_tbl[] )( void ) = {
#if !defined(USE_QUART_IN)
		// Raw
		accl_raw_init,

		//magn_raw_init,
		//gyro_raw_init,	// -yoshida 20160709
		//pres_raw_init,
		//ppg_raw_init,
	#endif
		// Accelerometer
		accl_pow_init,
		accl_lpf_init,
		accl_hpf_init,
		accl_line_init,
		// Magnetometer
		magn_param_init,
		magn_calib_raw_init,
		magn_calib_soft_init,
		magn_calib_hard_init,
		magn_lpf_init,
		// Gyroscope
		gyro_lpf_init,
		gyro_hpf_init,
		// Util
		cyclic_sensor_init,
		// Rotation
		rot_vec_init,
		orientation_init,
		rot_grav_vec_init,
		gravity_init,
		rot_lpf_vec_init,
		gyro_posture_init,
		NULL
};

